myHRMService.service("ModuleResource", ["$resource", function ($resource)
{
    return $resource("/api/modules/:id",
        { id: "@id" },
        {
            update: { method: "PUT" },
            query : {method : "GET",isArray:false }
        });
}]).service("ModuleService", ["$q", "$filter", "ModuleResource", function ($q, $filter, ModuleResource)
{
    var modules = null;
    currentModule = null;
    currentModuleId = null;
    this.get = function (params) {
        var defer = $q.defer();
        if(currentModule && currentModuleId === id)
        {
            defer.resolve(currentModule);
        }
        else {
            ModuleResource.get(params, function (data)
            {
                currentModule = data.data;
                currentModuleId = params.id ? params.id : null;
                defer.resolve(currentModule);
            });
        }
        return defer.promise;
    };

    this.query = function (params) {
        var defer = $q.defer();
        if(modules && false)
        {
            defer.resolve(modules);
        }
        else
        {
            ModuleResource.query(params).$promise.then(function (data) {
                modules = data;
                defer.resolve(modules);
            });
        }

        return defer.promise;
    };

    this.delete = function(module)
    {
        var defer = $q.defer();
        var temp = new ModuleResource();
        temp.$delete({ id: module.id }, function (data) {
            defer.resolve(modules);
        });
    }

    this.create = function (module) {
        var defer = $q.defer();
        var temp = new ModuleResource(module);
        temp.$save(null, function success(data)
        {
            defer.resolve(modules);
        },
         function error(data) {
             defer.reject(data);
         });
        return defer.promise;
    };

    this.update = function (module) {
        var defer = $q.defer();
        var found = module;
        //if(!currentModule || module.id !== currentModule.id)
        //{
        //    found = $filter('filter')(modules.data, { id: id }, true);
        //    if(found && found.length === 1)
        //    {
        //        found = found[0];
        //    }
        //}

        var temp = new ModuleResource(found);
        temp.$update({ id:module.Id}, function (data) {
            found = data;
            debugger;
            if (data.success)
            {
                defer.resolve(found);
            }
            else
            {
                defer.reject(found);
            }
            
        }, function (error)
        {
            defer.reject(found);
        });
        return defer.promise;

    };



}]);
