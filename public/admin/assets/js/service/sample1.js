myHRMService.service("JobTitleResource", ["$resource", function ($resource) {
    return $resource("/api/jobtitle/:id",
        { id: "@id" },
        {
            update: { method: "PUT" },
            query: { method: "GET", isArray: false }
        });
}]).service("JobTitleService", ["$q", "$filter", "JobTitleResource", function ($q, $filter, JobTitleResource) {
    var jobTitles = null;
    currentJobTitle = null;
    currentJobTitleId = null;
    this.get = function (params) {
        var defer = $q.defer();
        if (currentJobTitle && currentJobTitleId === id) {
            defer.resolve(currentJobTitle);
        }
        else 
        {
            JobTitleResource.get(params, function (data) {
                currentJobTitle = data.data;
                currentJobTitleId = params.id ? params.id : null;
                defer.resolve(currentJobTitle);
            });
        }
        return defer.promise;
    };

    this.query = function (params) {
        var defer = $q.defer();
        
        JobTitleResource.query(params).$promise.then(function (data) 
        {
            defer.resolve(data);
        });
        return defer.promise;
    };

    this.delete = function (jobTitle) {
        var defer = $q.defer();
        var temp = new JobTitleResource();
        temp.$delete({ id: jobTitle.Id }, function (data) {
            defer.resolve(data);
        });
        return defer.promise;
    }

    this.create = function (jobTitle) {
        var defer = $q.defer();
        var temp = new JobTitleResource(jobTitle);
        temp.$save(null, function success(data) 
        {
            if(data.success)
            {
                defer.resolve(data);    
            }
            else{
                defer.reject(data);
            }
            
        },
         function error(data) 
         {
             defer.reject(data);
         });
        return defer.promise;
    };

    this.update = function (jobTitle) {
        var defer = $q.defer();
        var found = jobTitle;
        var temp = new JobTitleResource(found);
        temp.$update({ id: jobTitle.Id }, function (data) {
            found = data;
            if (data.success) 
            {
                defer.resolve(found);
            }
            else {
                defer.reject(found);
            }

        }, function (error) {
            defer.reject(found);
        });
        return defer.promise;

    };



}]);
