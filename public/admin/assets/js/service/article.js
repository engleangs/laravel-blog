KhmerBlogService.service("ArticleResource", ["$resource", function ($resource) {
    return $resource("administrator/category/:id",
        { id: "@id" },
        {
            update: { method: "PUT" },
            query: { method: "GET", isArray: false }
        });
}]).service("ArticleService", ["$q", "$filter", "ArticleResource", '$http', function ($q, $filter, CategoryResource,$http) 
{
    var categorys       = null;
    currentCategory     = null;
    currentCategoryId   = null;
    var id              = 0;
    this.statuses         = [
            { id :1,name : "Published"},
            { id :0,name : "Drafted"},
            { id :-1,name :"Trashed"},
    ];
    this.getParent = function(categoryId)
    {
        var defer = $q.defer();
        $http.get('administrator/category/parent/'+categoryId).then(
            function success(data)
            {
                defer.resolve(data);
            },
            function error(data){
                defer.reject(data);
            });
        return defer.promise;
    };

    this.get = function (params) 
    {
        var defer = $q.defer();
        // if (currentCategory && currentCategoryId === id) 
        // {

        //     defer.resolve({'success':true,'data':currentCategory});
        // }
        // else 
        // {
            CategoryResource.get(params, function (data) 
            {
                currentCategory = data.data;
                id = params.id;
                currentCategoryId = params.id ? params.id : null;
                defer.resolve(data);
            });
        // }
        return defer.promise;
    };

    this.query = function (params) {
        var defer = $q.defer();
        
        CategoryResource.query(params).$promise.then(function (data) 
        {
            defer.resolve(data);
        });
        return defer.promise;
    };

    this.delete = function (category) {
        var defer = $q.defer();
        var temp = new CategoryResource();
        temp.$delete({ id: category.id }, function (data) {
            defer.resolve(data);
        });
        return defer.promise;
    }

    this.create = function (category) {
        var defer = $q.defer();
        var temp = new CategoryResource(category);
        temp.$save(null, function success(data) 
        {
            if(data.success)
            {
                defer.resolve(data);    
            }
            else{
                defer.reject(data);
            }
            
        },
         function error(data) 
         {
             defer.reject(data);
         });
        return defer.promise;
    };

    this.update = function (id,category) {
        var defer = $q.defer();
        var found = category;
        var temp = new CategoryResource(found);
        temp.$update({ id: id }, function (data) {
            found = data;
            if (data.success) 
            {
                defer.resolve(found);
            }
            else {
                defer.reject(found);
            }

        }, function (error) {
            defer.reject(found);
        });
        return defer.promise;

    };



}]);
