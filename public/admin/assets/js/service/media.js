KhmerBlogService.service("MediaResource", ["$resource", function ($resource) {
    return $resource("administrator/media/:id",
        { id: "@id" },
        {
            update: { method: "PUT" },
            query: { method: "GET", isArray: false }
        });
}]).
service("MediaService", ["$q", "$filter",'$http','MediaResource', function ($q, $filter,$http,MediaResource)
{
    var categorys       = null;
    currentCategory     = null;
    currentCategoryId   = null;
    var uploadUrl       = "administrator/media/upload";
    var id              = 0;
    this.get = function (params) 
    {
        var defer = $q.defer();
        $http.get('administrator/media').then(
            function success(data)
            {
                defer.resolve(data);
            },
            function error(data){
                defer.reject(data);
            });

        return defer;
    };

    this.query = function(params)
    {
        var defer = $q.defer();
        MediaResource.query(params).$promise.then(function (data) 
        {
            defer.resolve(data);
        });
        return defer.promise;

    };
    
    this.uploadFile = function (file) 
    {
        var defer = $q.defer();
          $http.post(uploadUrl, file, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
               })
               .success(function(data)
               {
                    defer.resolve(data);
               })
               .error(function(error)
               {
                 defer.reject(error);
               });  
        return defer.promise;
    };

    this.create = function (media)
    {
        var defer = $q.defer();
        var temp = new MediaResource(media);
        temp.$save(null, function success(data)
            {
                if(data.success)
                {
                    defer.resolve(data);
                }
                else{
                    defer.reject(data);
                }

            },
            function error(data)
            {
                defer.reject(data);
            });
        return defer.promise;
    };
    this.update = function(id,media) 
    {
        var defer       = $q.defer();
        var resoruce    = new MediaResource(media);
        resoruce.$update( { id: id }, function (data) 
            {
                found       = data;
                if (data.success) 
                {
                    defer.resolve(found);
                }
                else {
                    defer.reject(found);
                }

            }, function (error) 
            {
                defer.reject(error);
            }
        );
        return defer.promise;
    }

    this.delete = function(media)
    {
            var defer = $q.defer();
        var resoruce = new MediaResource();
        resoruce.$delete({ id: media.id }, function (data) 
        {
            defer.resolve(data);
        });
        return defer.promise;
    }

}]);
