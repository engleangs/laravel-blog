KhmerBlogService.service("HelperService",[function  () 
{
	this.activateTabError = function($error)
	{
		 $.each($error,function(inx,value)
		 {
                        var foundError = false;
                        $.each(value,function(k, objContr)
                        {
                             var elementName     = objContr.$name;
                            var lstCurrentTab  = $('[name="'+elementName+'"]').closest('.tab-pane');
                            console.log(lstCurrentTab);
                            if(lstCurrentTab.length > 0)
                            {
                                var currentTab = lstCurrentTab[0];
                                var identify = $(currentTab).attr('id');
                                $('a[href="#'+identify+'"]').trigger('click');
                                return false;

                            }
                            foundError = true;
                            return false;
                        });

                        if(foundError)
                        {
                            return false;
                        }
                    });
	};
	
	this.getLength = function(obj)
	{
		var keys = Object.keys(obj);
		return keys.length;
	};
}
]);