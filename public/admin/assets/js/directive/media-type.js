angular.module("KhmerBlog")
.directive('mediaTypeDirective',function($parse) 
{
	 return {
        require: "ngModel",
        restrict: 'A',
        link: function ($scope, el, attrs) 
        {
            var $element = $(el);
            var mimeTypeIcons = {
                'application/msword'    :  'fa fa-file-text-o fa-5x',
                'text/plain'            :  'fa fa-file-text-o fa-5x',
                'application/excel'     :  'fa fa-file-excel-o fa-5x',
                'application/x-msexcel' :  'fa fa-file-excel-o fa-5x',
                'application/vnd.ms-excel' : 'fa fa-file-excel-o fa-5x',
                'application/pdf'       :  'fa fa-file-pdf-o fa-5x',
                'text/html'             : 'fa fa-html5 fa-5x',
                'text/xml'              : 'fa fa-html5 fa-5x',

            };
            $scope.$watch(attrs.ngModel,function(newVal,oldVal)
            {
                if(newVal==null)
                {
                    el.html(''); // clear all iamge or document icon
                }    
                else
                {
                    prepareMediaItem(newVal);
                }
            });
            var folderClickCallBack = attrs.folderClick;

            function prepareMediaItem(mediaItem)
            {
                var popoverOpt = {
                    html :true,
                    trigger :'hover , click',
                    placement : 'right'
                };

                switch(mediaItem.type)
                {
                    case 'image':
                         var str = '<img src="'+_baseURL +mediaItem.path+'" title="Media Info"  width="50" '
                                    +'data-content="'+getImageInfo(mediaItem)+'"  class="thumbnail" />';
                        var jqElement = $(str);
                         el.html('');
                         jqElement.appendTo(el);
                         jqElement.popover( popoverOpt );

                        break;
                    case 'doc':
                        var icon = getDocIconClass(mediaItem.mime_type);
                        debugger;
                        var str  = '<div class="doc" title="Media Info"  '
                                    +'data-content="'+getMetaInfo(mediaItem)+'"><i class="'+icon+'"></i></div>';
                        el.html('');
                        var jqElement = $(str);
                        jqElement.appendTo(el);
                        jqElement.popover( popoverOpt);
                        break;
                    case 'folder':
                        var icon = '<i class="fa fa-folder-open fa-5x"></i>';
                        var str = '<div class="doc" title="Click to browse this folder">'+icon+'</div>';
                        el.html(''); //clear html element
                        var jqElement = $(str);
                        jqElement.tooltip({placement:"right"});

                        jqElement.click(function()
                            {
                                debugger;
                                var callBack = $scope[folderClickCallBack];
                                if(callBack && angular.isFunction(callBack)){
                                    callBack(mediaItem);
                                }
                            }
                        );
                        jqElement.appendTo(el);

                        break;
                    default :
                        break;
                }
            }
            
            function getImageInfo(mediaItem) 
            {
                var str = '<table>'
                            +'<tr><td> <img  src=\''+_baseURL+mediaItem.path+'\' class=\'thumbnail\'> </td></tr>'
                            +'</table>';
                return str;
            }

            function getMetaInfo(mediaItem)
            {
                var str = '<table valing=\'top\'> '
                            +'<tr><td width=\'100\' > Size  </td> <td> : </td> <td> '+mediaItem.size  +'</td></tr>';
                str += ' <tr> <td> Created At </td> <td> : </td> <td> '+mediaItem.created_at    +' </td> </tr>';
                str += ' <tr> <td> Created By </td> <td> : </td> <td>' + mediaItem.created_by +'</td> </tr> ';
                //str += ' <tr><td> Tmp Name    </td>   <td> : </td> <td>'+mediaItem.file   +'</td> </tr>';
                str += '</table>';
                return str;

            }

            function getDocIconClass (mimeType)
            {
                if(mimeTypeIcons[mimeType])
                {
                    return mimeTypeIcons[mimeType];
                }  
                return 'fa fa-file';
            }
        }
    };
});