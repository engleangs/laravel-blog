angular.module("KhmerBlog")
.directive('mediaWrapperDirective',function($parse) 
{
	 return {
        require: "ngModel",
        restrict: 'A',
        link: function ($scope, el, attrs, ngModel) 
        {
            var $element = $(el);
            var mimeTypeIcons = {
                'application/msword'    :  'fa fa-file-text-o fa-5x',
                'text/plain'            :  'fa fa-file-text-o fa-5x',
                'application/excel'     :  'fa fa-file-excel-o fa-5x',
                'application/x-msexcel' :  'fa fa-file-excel-o fa-5x',
                'application/vnd.ms-excel' : 'fa fa-file-excel-o fa-5x',
                'application/pdf'       :   'fa fa-file-pdf-o fa-5x',
                'text/html'             : 'fa fa-html5 fa-5x',
                'text/xml'              : 'fa fa-html5 fa-5x',

            };

            $scope.$watch(attrs.ngModel,function(newVal,oldVal)
            {
                debugger;
                if(newVal==null)
                {
                    el.html(''); // clear all iamge or document icon
                }    
                else
                {
                    prepareMediaItem(newVal);
                }
            });

            function prepareMediaItem(mediaItem)
            {
                var popoverOpt = {
                    html :true,
                    trigger :'hover',
                    placement : 'right'
                };
                switch(mediaItem.type)
                {
                    case 'image':
                         var str = '<img src="'+mediaItem.url+'" title="Media Info"  '
                                    +'data-content="'+getMetaInfo(mediaItem)+'"  class="thumbnail" />';
                        var jqElement = $(str);
                         el.html('');
                         jqElement.appendTo(el);
                         jqElement.popover( popoverOpt );

                        break;
                    case 'doc':
                        var icon = getDocIconClass(mediaItem.mimeType);
                        var str  = '<div class="doc" title="Media Info"  '
                                    +'data-content="'+getMetaInfo(mediaItem)+'"><i class="'+icon+'"></i></div>';
                        el.html('');
                        var jqElement = $(str);
                        jqElement.appendTo(el);
                        jqElement.popover( popoverOpt);
                        break;
                    default :
                        break;
                }
            }
            

            function getMetaInfo(mediaItem)
            {
                var str = '<table valing=\'top\'> '
                            +'<tr><td width=\'100\' > Size  </td> <td> : </td> <td> '+mediaItem.size          +'</td></tr>';
                str += ' <tr><td> Client Name </td> <td> : </td> <td> '+mediaItem.clientName    +'</td></tr>';
                str += ' <tr><td> Tmp Name    </td>   <td> : </td> <td>'+mediaItem.fileName       +'</td> </tr>';
                str += '</table>';
                return str;

            }

            function getDocIconClass (mimeType)
            {
                if(mimeTypeIcons[mimeType])
                {
                    return mimeTypeIcons[mimeType];
                }  
                return 'fa fa-file';
            }
        }
    };
});