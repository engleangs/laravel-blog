angular.module("KhmerBlog")
.directive('bindFileDirective',function($parse) 
{
	 return {
        require: "ngModel",
        restrict: 'A',
        link: function ($scope, el, attrs, ngModel) 
        {
        	var attrHandler = $parse(attrs['fileChange']);

        	if(attrHandler)
        	{
        		 // This is a wrapper handler which will be attached to the
		        // HTML change event.
		        var handler = function (e) {

		          $scope.$apply(function () {

		            attrHandler($scope, { $event: e, files: e.target.files });
		          });
		        };

	        
	        	el[0].addEventListener('change', handler, false);
        	}

        	// el.bind('change', function (event) 
        	// {
        	// 	console.log(event);
         //        ngModel.$setViewValue(event.target.files[0]);
         //        $scope.$apply();
         //    });
            
         //    $scope.$watch(function () {
         //        return ngModel.$viewValue;
         //    }, function (value) {
         //        if (!value) {
         //            el.val("");
         //        }
         //    });
	       
           
        }
    };
});