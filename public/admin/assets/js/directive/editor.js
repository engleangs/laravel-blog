angular.module("KhmerBlog")
.directive('ckeditorDirective',function($parse) 
{
	 return {
        require: "ngModel",
        restrict: 'A',
        link: function ($scope, el, attrs) 
        {
        	var element 	= $(el);
          var parent    = element.parent();
          var ckeditor  = null;
          var button    = $("<button type='button' class='btn btn-default btn-sm'> Insert Image</button>");
          button.click(function(){
            if( ckeditor ) {
              var img   = "<img src='https://oleweidner.com/blog//assets/images/splash/photo-1415757463766-a0ff23175cd4.jpg' />";
              ckeditor.insertHtml(img);  
            }
          });
          button.insertAfter( element );
        	ckeditor 	= CKEDITOR.replace(element.attr('id'));
        	var modelName 	= attrs.ngModel;
        	 ckeditor.on("change",function(e)
            {
              ckeditor.updateElement();
              var contentHtml = ckeditor.getData();
               $scope.$eval(modelName +"='"+contentHtml+"'");
               $scope.$apply();
            });
        	
        }
    }
});