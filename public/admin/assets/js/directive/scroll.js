angular.module("KhmerBlog")
.directive('scrollWrapperDirective',function($parse) 
{
	 return {
        //require: "ngModel",
        restrict: 'AE',
        link: function ($scope, el, attrs) 
        {
        	var element 	= $(el);
          var items     = [];
          var width     = element.closest('.form-horizontal').width();
          element.find('.scroll-head').width(width);
          var scPostion = element.find('.scroll-head').position();
          var deltaTop  = 50;
           element.find( 'nav ul li a').each( function (){
              $(this).click( function(){
                var target = $(this).attr('data-target');
                if(target)
                {
                   var position =  $('#'+target).position();
                   $('#body-container').animate( {scrollTop  : position.top - deltaTop  } ,{
                    speed : 500,
                    easing : 'swing',
                   });
                   element.find('.scroll-head').find('.active').removeClass('active');
                   $(this).parent().addClass('active');
                   $(this).closest('.dropdown').addClass('active');

                }
              });
            });

          element.find('.scroll-item').each(function(){
            var item = {
              element : this,
              position : $(this).position()
            };
             items.push( item );
          });
          items.sort(function(a,b)
          {
            var topA = a.position.top;
            var topB = b.position.top;
            return topA - topB;
          });
          var counter = 0 ;
          window.bodyContainerScrollObserable = window.bodyContainerScrollObserable || [];
          var scrollContainerCallback = function (container, event, scrollTop) 
          {
            if(scrollTop > 0)
            {
              var left = (scPostion.left - 30);
              element.find('.scroll-head').css({'top':'50px','left': left +'px' }).width(width + 60);
              $('#btn-top-group').find('button').removeClass('btn-xs');
              $('#btn-top-group').css({ top : '60px'});
              $('#btn-cancel').addClass('btn-default').removeClass('btn-primary');
            }
            else
            {
             element.find('.scroll-head').css({'top':'120px','left':scPostion.left+'px'}).width( width); 
             $('#btn-top-group').find('button').addClass('btn-xs');
             $('#btn-top-group').css({ top : '70px'});
             $('#btn-cancel').addClass('btn-primary').removeClass('btn-default');
            }
              for(var i  in items)
              {
                  var item = items[ i ];
                  if( item.position.top + deltaTop  > scrollTop  ) 
                  {
                    console.log( scrollTop , item);
                    var itemId = $(item.element).attr('id');
                    // $(item.element).find('[data-toggle ="tab"]').each(function(){
                    //   if($(this).hasClass('active'))
                    //   {
                    //     $(this).trigger( 'click' );
                    //   }
                    // });
                    console.log( itemId );
                    element.find('.scroll-head').find('.active').removeClass('active');
                    $('[data-target="'+itemId+'"]').parent().addClass('active').closest('.dropdown').addClass('active'); 
                    
                    break;
                  }
              }
              //console.log( ' on scroll  from call back ', counter ++ , 'scroll top : ',scrollTop);
          };
          window.bodyContainerScrollObserable.push( scrollContainerCallback );

          function prepareForNavigation()
          {
           
          }
        }
    }
});