angular.module("MyHRM")
.controller("JobTitleCtrl", ["$scope", "JobTitleService", "$q", "NgTableParams", "$uibModal", "$rootScope","alertify",
    function ($scope, JobTitleService, $q, NgTableParams, $uibModal, $rootScope,alertify) {
        $scope.data = $scope.data || {};
        $scope.data.modules = {};
        $scope.data.moduleQuery = "";
        $scope.data.moduleTitle = "Add New JobTitle";
        $scope.selectedJobTitle = null;
        var that = this;

        var getModalInstance = function () {
            return $uibModal.open({
                animation: true,
                templateUrl: "/JobTitle/Edit",
                controller: "JobTitleModalCtrl",
                bindToController: true,
                size: "",
                resolve: {
                    item: function () {
                        return $scope.selectedJobTitle;
                    }
                }
            });
        }

        $scope.addJobTitle = function () {
            $scope.selectedJobTitle = null;
            modalInstance = getModalInstance();
            modalInstance.result.then(function (result) {

                if (result.success) {
                    that.tableParams.reload();
                }
            });
        };

        $scope.editJobTitle = function (jobTitle) 
        {
            $scope.selectedJobTitle = jobTitle;
            modalInstance = getModalInstance();
            modalInstance.result.then(function (jobTitle) 
            {
                that.tableParams.reload();
                
            });
        };

        $scope.deleteJobTitle = function(jobTitle)
        {
            alertify.confirm("Are you sure you want to delete Job Title ?",
                function confirm() {
                    var jobTitleDeletePromise = JobTitleService.delete(jobTitle);
                    jobTitleDeletePromise.then(function success(data){
                        if(data.success)
                        {
                            alertify.success("Job Title has been deleted Successfully.");
                            that.tableParams.reload();   
                        }
                        
                    },function error(data)
                    {
                        if(data.message)
                        {
                            alertify.error(data.message);
                        }
                    });
            }, function cancel(){

            });
        }

        this.tableParams = new NgTableParams({}, {
            getData: function (params) {

                $rootScope.showLoader = true;
                var jobTitleQueryPromise = JobTitleService.query({
                    option: {
                        PerPage: params.count(),
                        Page: params.page(),
                        Filter: params.filter(),
                        Sorting: params.sorting(),
                    }
                });
                return jobTitleQueryPromise.then(function (modules) {
                    params.total(modules.total);
                    $rootScope.showLoader = false;
                    return modules.data;

                });
            }
        });


       
        $scope.finishLoading = function () {
            console.log('finish loading');
        };
      

    }]).controller("JobTitleModalCtrl", ["$scope", "$http", "$q",
        "$uibModalInstance", "item", "$rootScope", "JobTitleService", "alertify",
        function ($scope, $http, $q, $uibModalInstance, item, $rootScope, JobTitleService, alertify) {
            $scope.selectedJobTitle =  angular.copy(item);
            var backUpModule = angular.copy(item);
            $scope.isEdit = item != null;
            $scope.getModuleTitle = function () {
                if (!$scope.isEdit) {
                    return " Add New JobTitle";
                }
                return " Edit JobTitle";
            }
            $scope.ok = function () 
            {
               
                var closeDialog = function (success) {
                    $uibModalInstance.close({
                        success: success
                    });
                }
                
                $rootScope.showLoader = true;
                if (item == null) {


                    var createPromise = JobTitleService.create($scope.selectedJobTitle);
                    createPromise.then(function success(data) {
                        $rootScope.showLoader = false;
                        alertify.success("Successfully");
                        closeDialog(true);
                    }, function error(data) {
                        $rootScope.showLoader = false;
                        alertify.error("Sorry error occured!");
                        closeDialog(false);
                    }
                    );
                }
                else
                 {
                    var updatePromise = JobTitleService.update($scope.selectedJobTitle);
                    updatePromise.then(function () {
                        $rootScope.showLoader = false;
                        alertify.success("Successfully");
                        closeDialog(true);
                    }, function () {
                        $rootScope.showLoader = false;
                        $scope.selectedJobTitle = backUpModule;
                        alertify.error("Sorry error occured!");
                        closeDialog(false);
                    });
                }


            };
            $scope.cancel = function () {
                $scope.selectedJobTitle = $scope.backUpModule;
                $uibModalInstance.dismiss('cancel');
            };
            

        }]);