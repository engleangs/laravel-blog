 angular.module("KhmerBlog")
.controller("MediaCtrl", ["$scope", "MediaService", "$q", "NgTableParams", "$uibModal", "$rootScope","alertify",
    function ($scope, MediaService, $q, NgTableParams, $uibModal, $rootScope,alertify) {
        $scope.data             = $scope.data || {};
        $scope.data.modules     = {};
        $scope.data.moduleQuery = "";
        $scope.data.moduleTitle = "Add New Media";
        $scope.selectedMedia    = null;
        var that                = this;
        $scope.filter           = {};
        $scope.parent_id        = 0;
        $scope.folder           = "";
        $scope.currentFolders   = [];
        $scope.$watch("filter.name",function(newVal,oldVal)
        {
            that.search();
        });

        $scope.searchLanguage   = null;
        $scope.onClearSelectLanguage = function()
        {
            $scope.filter.language  = undefined;
            that.search();
        }
        
        $scope.onSelectLanguage = function (item, model)
        {
            that.search();
        };
        
        this.search             = function()
        {
            that.tableParams.reload();   
        };

        $scope.navigateToFolder = function(item,isRoot)
        {

            if( isRoot )
            {
                $scope.currentFolders   = [];
                $scope.parent_id        = 0;
            }
            else 
            {
                debugger;
                if( item.id != $scope.parent_id ) 
                {
                    for(var i = $scope.currentFolders.length -1 ; i >= 0 ;i--) 
                    {
                        var folder = $scope.currentFolders[ i ];
                        if(folder.id != item.id )
                        {
                            var folders             = $scope.currentFolders.splice(0, i );
                            $scope.currentFolders   = folders;

                        }
                        else
                        {
                            break;
                        }
                    }
                    $scope.parent_id = item.id;
                    $scope.folder    = item.path ; // store folder for adding new folder/ media to inside it
                }


            }
            that.tableParams.reload();  

        }

        $scope.gotoFolder       = function( item)
        {
            $scope.parent_id    =  item.id ;
            $scope.currentFolders.push( item );
            that.tableParams.reload();
            $scope.folder       = item.path; //store folder for adding new folder / media to be inside it
        };
        var getModalInstance = function () {
            return $uibModal.open({
                animation: true,
                templateUrl: "administrator/template/edit-media",
                controller: "MediaModalCtrl",
                bindToController: true,
                size: "",
                resolve: {
                    item: function ()
                    {
                        return {
                                parent_id : $scope.parent_id ,
                                media     : $scope.selectedMedia,
                                folder    : $scope.folder,
                                type      : $scope.type,
                            };

                    }
                }
            });
        }

        $scope.addMedia = function () {
            $scope.selectedMedia = null;
            $scope.type = 'media',
            modalInstance = getModalInstance();
            modalInstance.result.then(function (result) {

                if (result.success) {
                    that.tableParams.reload();
                }
            });
        };
        $scope.addFolder = function(){
            $scope.selectedMedia = null;
            $scope.type = 'folder';
            modalInstance = getModalInstance();
            modalInstance.result.then(function(result){
                if( result.success ){
                    that.tableParams.reload();
                }
            });
        };

        $scope.editMedia = function (mediaItem) 
        {
            $scope.selectedMedia    = mediaItem;
            $scope.type             = mediaItem.type =='folder'? mediaItem.type : 'media'; 
            modalInstance           = getModalInstance(); 
            modalInstance.result.then(
                function (media) 
                {
                    that.tableParams.reload();
                }
            );      
        };

        $scope.deleteMedia = function(item)
        {
            var title = item.type=='folder'? "Are you sure you want to delete this fodler ?" : " Are you sure you want to delete this media";
            alertify.confirm( title,
                function confirm() {
                    debugger;
                    var mediaDeletePromise = MediaService.delete(item);
                    mediaDeletePromise.then(function success(data)
                    {
                        if(data.success)
                        {
                            alertify.success("Media has been deleted Successfully.");
                            that.tableParams.reload();   
                        }
                        else
                        {
                            var msg = 'Sorry error occured !';
                            if(data.message)
                            {
                                msg =  data.message;
                                
                            }
                            alertify.error(msg);   
                        }
                        
                    },function error(data)
                    {
                        var msg = 'Sorry error occured !';
                        if(data.message)
                        {
                            msg =  data.message;
                            
                        }
                        alertify.error(msg);
                        
                    });
            }, function cancel(){

            });
        }
        

        this.tableParams = new NgTableParams(
            {
                    count : 5,
                    total : 100
            }, 
            {
                paginationMaxBlocks: 13,
                paginationMinBlocks: 1,
                counts: [5,10,20],
                getData: function (params) 
                {
                    $rootScope.showLoader = true;
                    var filter   = {};
                    if($scope.filter.name)
                    {
                        filter.name = $scope.filter.name;
                    }
                    if($scope.filter.language)
                    {
                        filter.language = $scope.filter.language.id;
                    }
                    filter.parent_id = $scope.parent_id;
                    console.log($scope.filter);

                    var mediaQueryPromise = MediaService.query({
                        option: {
                            per_page: params.count(),
                            page: params.page(),
                            filter: filter,
                            sorting: params.sorting(),
                        }
                    });
                    
                    return mediaQueryPromise.then(function (modules) 
                    {
                        params.total(modules.total);//update paginate table
                        $rootScope.showLoader = false;
                        return modules.data;

                    },function error(data)
                    {
                        console.log('error getting data');
                        $scope.showLoader = false;
                    });
                }
        });


        $scope.finishLoading = function () {
            console.log('finish loading');
        };
      

    }]).controller("MediaModalCtrl", ["$scope", "$http", "$q",
        "$uibModalInstance", "item", "$rootScope", "MediaService", "alertify", "HelperService",
        function ($scope, $http, $q, $uibModalInstance, item, $rootScope, MediaService, alertify,HelperService) 
        {

            var backUpModule        = angular.copy(item);
            $scope.selectedMedia    = backUpModule.media ==null?{}:backUpModule.media;
            $scope.parent_id        = backUpModule.parent_id;
            $scope.isEdit           = false;
            var itemId              = $scope.selectedMedia.id || 0;
            $scope.type             = backUpModule.type ||'media';
            var mediaTitles         = {
                'add':{
                    'media'     : ' Add New Media',
                    'folder'    : ' Add New Folder',
                },
                'edit' : {
                    'media'     : ' Edit Media',
                    'folder'    : ' Edit Folder',
                }
            };

            $scope.isEdit = backUpModule.media !=null ; // if there is media exist
            function getFolderPath(path,file)
            {
                var str = path;
                str = str.replace("/"+file, "");
                return str;

            }

            if(backUpModule.media !=null )
            {
                var mediaItem = {
                    fileName    : $scope.selectedMedia.file,
                    folderPath  :  getFolderPath($scope.selectedMedia.path,$scope.selectedMedia.file),
                    type        : $scope.selectedMedia.type,
                    url         : _baseURL+'/'+$scope.selectedMedia.path, 
                }
                $scope.mediaItem = mediaItem;
            }

            $scope.uploadFile = function () 
            {
               $('#file-media').trigger('click');
                 
            }

            $scope.uploadFileChange = function(objEl,files)
            {
                var fd = new FormData();
                fd.append('media', files[0]);
                var promise  = MediaService.uploadFile(fd);
                promise.then(function success(data)
                {
                    debugger;
                    if(data.success)
                    {
                        $scope.mediaItem                = data;
                        $scope.selectedMedia.tmpPath    = data.folderPath+"/"+data.fileName;
                        $scope.selectedMedia.mime_type  = data.mimeType;
                    }
                    else
                    {
                        alertify.error(data.message);
                    }
                },function error(data)
                {
                    console.log(data);
                });

                $('#file-media').closest('form')[0].reset();

            };


            $scope.getModuleTitle = function ()
            {
                var title = mediaTitles['edit'];
                if (!$scope.isEdit)
                {
                    title = mediaTitles['add'];
                }

                return title[$scope.type];
            }

            $scope.ok = function (form) 
            {
                debugger;

                if(HelperService.getLength(form.$error)> 0)
                {
                   HelperService.activateTabError(form.$error);
                   return;
                }
               

                var closeDialog = function (success) {
                    $uibModalInstance.close({
                        success: success
                    });
                }
                $rootScope.showLoader = true;
                if($scope.type == 'folder')
                {
                    $scope.selectedMedia.type = $scope.type; //type is folder then set it here
                }

                if (item.media == null)
                {
                    // if add new media
                    $scope.selectedMedia.parent_id  = backUpModule.parent_id;
                    $scope.selectedMedia.folder     = backUpModule.folder;
                    var createPromise               = MediaService.create($scope.selectedMedia);
                    createPromise.then(function success(data)
                        {
                        $rootScope.showLoader = false;
                        alertify.success("Successfully");
                        closeDialog(true);
                    }, function error(data) 
                    {
                        $rootScope.showLoader = false;
                        var message = data.message || 'Sorry error occured';
                        alertify.error(message);
                        //closeDialog(false);
                    }
                    );
                }
                else
                 {
                    debugger;
                    var updatePromise = MediaService.update(itemId,$scope.selectedMedia);
                    updatePromise.then(function () {
                        $rootScope.showLoader = false;
                        alertify.success("Successfully");
                        closeDialog(true);
                    }, function () {
                        $rootScope.showLoader = false;
                        $scope.selectedMedia = backUpModule;
                        alertify.error("Sorry error occured!");
                        closeDialog(false);
                    });    
                    

                    
                }


            };
            $scope.cancel = function () {
                $scope.selectedMedia = $scope.backUpModule;
                $uibModalInstance.dismiss('cancel');
            };

            

        }]);