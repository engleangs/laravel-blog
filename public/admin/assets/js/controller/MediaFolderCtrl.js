angular.module("KhmerBlog")
.controller("MediaModalCtrl", ["$scope", "$http", "$q",
        "$uibModalInstance", "item", "$rootScope", "MediaService", "alertify", "HelperService",
        function ($scope, $http, $q, $uibModalInstance, item, $rootScope, MediaService, alertify,HelperService) 
        {
        	var backUpModule        = angular.copy(item);
            $scope.selectedMedia    = backUpModule.media ==null?{}:backUpModule.media;
            $scope.parent_id        = backUpModule.parent_id;
            $scope.isEdit           = false;
            var itemId              = $scope.selectedMedia.id || 0;
            $scope.isEdit = backUpModule.media !=null ; // if there is media exist
            $scope.rootPath 		= backUpModule.rootPath;
            
        });