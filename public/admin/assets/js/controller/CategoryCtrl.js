angular.module("KhmerBlog")
.controller("CategoryCtrl", ["$scope", "CategoryService", "$q", "NgTableParams", "$uibModal", "$rootScope","alertify",
    function ($scope, CategoryService, $q, NgTableParams, $uibModal, $rootScope,alertify) {
        $scope.data = $scope.data || {};
        $scope.data.modules = {};
        $scope.data.moduleQuery = "";
        $scope.data.moduleTitle = "Add New category";
        $scope.selectedCategory = null;
        var that = this;
        $scope.filter = {};
        $scope.$watch("filter.name",function(newVal,oldVal)
        {
            that.search();
        });
        $scope.searchLanguage = null;
        $scope.onClearSelectLanguage = function(){
            $scope.filter.language  = undefined;
            that.search();
        }
        
        $scope.onSelectLanguage = function (item, model)
        {
            that.search();
        };
        
        this.search  = function()
        {
            that.tableParams.reload();   
        };
        var getModalInstance = function () {
            return $uibModal.open({
                animation: true,
                templateUrl: "administrator/template/edit-category",
                controller: "CategoryModalCtrl",
                bindToController: true,
                size: "",
                resolve: {
                    item: function () {
                        
                        return $scope.selectedCategory;
                    }
                }
            });
        }

        $scope.addCategory = function () {
            $scope.selectedCategory = null;
            modalInstance = getModalInstance();
            modalInstance.result.then(function (result) {

                if (result.success) {
                    that.tableParams.reload();
                }
            });
        };

        $scope.editCategory = function (categoryItem) 
        {
            var getCategoryPromise      = CategoryService.get(categoryItem);
            $rootScope.showLoader       = true;
            getCategoryPromise.then(function success(data)
            {
                if(data.success)
                {
                    $scope.selectedCategory = data.data;
                    modalInstance           = getModalInstance(); 
                    
                    modalInstance.result.then(
                        function (category) 
                        {
                            that.tableParams.reload();
                        }
                    );   
                }
                $rootScope.showLoader   = false;
                    
            },function error(data)
            {
                $rootScope.showLoader   = false;
            });
            
            
        };

        $scope.deleteCategory = function(category)
        {
            alertify.confirm("Are you sure you want to delete Category?",
                function confirm() {

                    var categoryDeletePromise = CategoryService.delete(category);
                    categoryDeletePromise.then(function success(data){
                        
                        if(data.success)
                        {
                            alertify.success("Category has been deleted Successfully.");
                            that.tableParams.reload();   
                        }
                        else
                        {
                            var msg = 'Sorry error occured !';
                            if(data.message)
                            {
                                msg =  data.message;
                                
                            }
                            alertify.error(msg);   
                        }
                        
                    },function error(data)
                    {
                        var msg = 'Sorry error occured !';
                        if(data.message)
                        {
                            msg =  data.message;
                            
                        }
                        alertify.error(msg);
                        
                    });
            }, function cancel(){

            });
        }
        

        this.tableParams = new NgTableParams(
            {
                    count : 5,
                    total : 100
            }, 
            {
                paginationMaxBlocks: 13,
                paginationMinBlocks: 1,
                counts: [5,10,20],
                getData: function (params) 
                {

                    $rootScope.showLoader = true;
                var filter   = {};
                if($scope.filter.name)
                {
                    filter.name = $scope.filter.name;
                }
                if($scope.filter.language)
                {
                    filter.language = $scope.filter.language.id;
                }

                var categoryQueryPromise = CategoryService.query({
                    option: {
                        per_page: params.count(),
                        page: params.page(),
                        filter: filter,
                        sorting: params.sorting(),
                    }
                });
                
                return categoryQueryPromise.then(function (modules) {
                    params.total(modules.total);//update paginate table
                    $rootScope.showLoader = false;
                    return modules.data;

                },function error(data){
                    $scope.showLoader = false;

                });
            }
        });


        $scope.finishLoading = function () {
            console.log('finish loading');
        };
      

    }]).controller("CategoryModalCtrl", ["$scope", "$http", "$q",
        "$uibModalInstance", "item", "$rootScope", "CategoryService", "alertify", "HelperService",
        function ($scope, $http, $q, $uibModalInstance, item, $rootScope, CategoryService, alertify,HelperService) 
        {
            $scope.selectedCategory =  angular.copy(item);
            var backUpModule = angular.copy(item);
            $scope.isEdit  = false;
            $scope.parentCategory = { 'id' : 1}; // always root
            var itemId     = 0;
            debugger;
            if(item)
            {
                $scope.isEdit  = true;
                for(var key in item)
                {
                    $scope.parentCategory.id = item[key].parent_id;
                    itemId = item[key].id;
                    break;
                }
                
                
            }
            var parentPromise   = CategoryService.getParent(itemId);
            

            parentPromise.then(function success(data)
            {
                $scope.lstParent = data.data;
                for(var key in $scope.lstParent.data)
                {
                    var parent = $scope.lstParent.data[key];
                    if(parent.id == $scope.parentCategory.id)
                    {
                        $scope.parentCategory.selected = parent;
                        break;
                    } 
                }
            }, function error(data)
            {
                $scope.lstParent = { data :[] };
            });
            $q.all([parentPromise]).then(function (data) 
            {
                console.log('finish loading parent');
            });

            $scope.getModuleTitle = function () {
                if (!$scope.isEdit) {
                    return " Add New Category";
                }
                return " Edit Category";
            }
            $scope.ok = function (form) 
            {
                console.log();

                if(HelperService.getLength(form.$error)> 0)
                {
                   HelperService.activateTabError(form.$error);
                   return;
                }
               

                var closeDialog = function (success) {
                    $uibModalInstance.close({
                        success: success
                    });
                }
                
                $rootScope.showLoader = true;
                $scope.selectedCategory.parent_id = $scope.parentCategory.selected.id;
                if (item == null) 
                {


                    var createPromise = CategoryService.create($scope.selectedCategory);
                    createPromise.then(function success(data) {
                        $rootScope.showLoader = false;
                        alertify.success("Successfully");
                        closeDialog(true);
                    }, function error(data) {
                        $rootScope.showLoader = false;
                        alertify.error("Sorry error occured!");
                        //closeDialog(false);
                    }
                    );
                }
                else
                 {
                    debugger;
                    var updatePromise = CategoryService.update(itemId,$scope.selectedCategory);
                    updatePromise.then(function () {
                        $rootScope.showLoader = false;
                        alertify.success("Successfully");
                        closeDialog(true);
                    }, function () {
                        $rootScope.showLoader = false;
                        $scope.selectedCategory = backUpModule;
                        alertify.error("Sorry error occured!");
                        closeDialog(false);
                    });
                }


            };
            $scope.cancel = function () {
                $scope.selectedCategory = $scope.backUpModule;
                $uibModalInstance.dismiss('cancel');
            };
            

        }]);