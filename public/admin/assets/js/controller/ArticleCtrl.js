angular.module("KhmerBlog")
.controller("ArticleCtrl", ["$scope", "ArticleService", "$q", "NgTableParams", "$uibModal", "$rootScope","alertify","CategoryService",
    function ($scope, ArticleService, $q, NgTableParams, $uibModal, $rootScope,alertify,CategoryService) {
        $scope.data             = $scope.data || {};
        $scope.data.modules     = {};
        $scope.data.moduleQuery = "";
        $scope.data.moduleTitle = "Add New Article";
        $scope.status           = ArticleService.statuses;
        $scope.categoryData     = {
            filter      :'',
            data        : {},
            currentPage : 1,
            per_page    : 10,
            has_next    : true,
            has_previous : true,

        };
        console.log( 'status ',$scope.status);
        $scope.selectedArticle = null;
        var that = this;
        $scope.filter = {};
        $scope.$watch("filter.name",function(newVal,oldVal)
        {
            that.search();
        });
        $scope.searchLanguage = null;
        $scope.onClearSelectLanguage = function(){
            $scope.filter.language  = undefined;
            that.search();
        }
        $scope.onSelectLanguage = function (item, model)
        {
            that.search();
        };
        
        this.search  = function()
        {
            that.tableParams.reload();   
        };
        var getModalInstance = function () 
        {
            return $uibModal.open({
                animation: true,
                templateUrl: "administrator/template/edit-category",
                controller: "ArticleModalCtrl",
                bindToController: true,
                size: "",
                resolve: {
                    item: function () {
                        
                        return $scope.selectedArticle;
                    }
                }
            });
        }

        $scope.addArticle = function () 
        {
            $scope.selectedArticle = null;
            modalInstance = getModalInstance();
            modalInstance.result.then(function (result) {

                if (result.success) {
                    that.tableParams.reload();
                }
            });
        };

        $scope.editArticle = function (categoryItem) 
        {
            var getCategoryPromise      = ArticleService.get(categoryItem);
            $rootScope.showLoader       = true;
            getCategoryPromise.then(function success(data)
            {
                if(data.success)
                {
                    $scope.selectedArticle = data.data;
                    modalInstance           = getModalInstance(); 
                    
                    modalInstance.result.then(
                        function (category) 
                        {
                            that.tableParams.reload();
                        }
                    );   
                }
                $rootScope.showLoader   = false;
                    
            },function error(data)
            {
                $rootScope.showLoader   = false;
            });
            
            
        };



        var searchCategory = function()
        {
            var categoryQueryPromise = CategoryService.query({
                    option: {
                        per_page    : 10,
                        page        : $scope.categoryData.page,
                        filter      : { name :  $scope.categoryData.filter ,language : 1 },
                    }
                });
            categoryQueryPromise.then( function(data)
            {
                $scope.categoryData.data = data;
                var total = $scope.categoryData.data.total || 0;
                $scope.categoryData.totalPage =  Math.ceil(  total / $scope.categoryData.per_page ) ;
                checkCategoryPage();
                
            },function( error ) {
                console.log( error );
            });
        }
        var checkCategoryPage = function(){
            if( $scope.categoryData.currentPage == 1) {
                $scope.categoryData.has_previous = false;
            }
            if( $scope.categoryData.currentPage == $scope.categoryData.totalPage ) 
            {
                $scope.categoryData.has_next = false;
            }
        }

        $scope.searchCategory = function()
        {
            $scope.categoryData.currentPage = 1;
            searchCategory();
            
        }

        $scope.categoryPageNext = function()
        {
            debugger;
            if( $scope.categoryData.totalPage > $scope.categoryData.currentPage) 
            {
                $scope.categoryData.currentPage++;
                searchCategory();
            }
        }
        $scope.categoryPagePrevious = function(){
            if( $scope.categoryData.currentPage > 1) {
                $scope.categoryData.currentPage--;
                searchCategory();
            }
        }

        $scope.$watch('categoryData.filter',function(newVal,oldVal){
            console.log(newVal);
            $scope.searchCategory();

        });

        $scope.deleteArticle = function(category)
        {
            alertify.confirm("Are you sure you want to delete Article?",
                function confirm() 
                {

                    var deletePromise   = ArticleService.delete(category);
                    deletePromise.then(function success(data){
                        
                        if(data.success)
                        {
                            alertify.success("Article has been deleted Successfully.");
                            that.tableParams.reload();   
                        }
                        else
                        {
                            var msg = 'Sorry error occured !';
                            if(data.message)
                            {
                                msg =  data.message;
                                
                            }
                            alertify.error(msg);   
                        }
                        
                    },function error(data)
                    {
                        var msg = 'Sorry error occured !';
                        if(data.message)
                        {
                            msg =  data.message;
                            
                        }
                        alertify.error(msg);
                        
                    });
            }, function cancel(){

            });
        }
        

        this.tableParams = new NgTableParams(
            {
                    count : 5,
                    total : 100
            }, 
            {
                paginationMaxBlocks: 13,
                paginationMinBlocks: 1,
                counts: [5,10,20],
                getData: function (params) 
                {

                    $rootScope.showLoader = true;
                    var filter   = {};
                    if($scope.filter.name)
                    {
                        filter.name = $scope.filter.name;
                    }
                    if($scope.filter.language)
                    {
                        filter.language = $scope.filter.language.id;
                    }

                    var queryPromise = ArticleService.query({
                        option: {
                            per_page: params.count(),
                            page: params.page(),
                            filter: filter,
                            sorting: params.sorting(),
                        }
                    });
                
                return queryPromise.then(function (modules) 
                {
                    params.total(modules.total);//update paginate table
                    $rootScope.showLoader = false;
                    return modules.data;

                },function error(data){
                    $scope.showLoader = false;
                    console.log( data ); 

                });
            }
        });


        $scope.finishLoading = function () {
            console.log('finish loading');
        };
      

    }]).controller("ArticleModalCtrl", ["$scope", "$http", "$q",
        "$uibModalInstance", "item", "$rootScope", "ArticleService", "alertify", "HelperService",
        function ($scope, $http, $q, $uibModalInstance, item, $rootScope, ArticleService, alertify,HelperService) 
        {
            $scope.selectedArticle     = angular.copy(item);
            var backUpModule            = angular.copy(item);
            $scope.isEdit               = false;
            $scope.parentCategory       = { 'id' : 1}; // always root
            var itemId                  = 0;
            debugger;
            if(item)
            {
                $scope.isEdit  = true;
                for(var key in item)
                {
                    $scope.parentCategory.id = item[key].parent_id;
                    itemId = item[key].id;
                    break;
                }
                
                
            }
            var parentPromise   = ArticleService.getParent(itemId);
            

            parentPromise.then(function success(data)
            {
                $scope.lstParent = data.data;
                for(var key in $scope.lstParent.data)
                {
                    var parent = $scope.lstParent.data[key];
                    if(parent.id == $scope.parentCategory.id)
                    {
                        $scope.parentCategory.selected = parent;
                        break;
                    } 
                }
            }, function error(data)
            {
                $scope.lstParent = { data :[] };
            });
            $q.all([parentPromise]).then(function (data) 
            {
                console.log('finish loading parent');
            });

            $scope.getModuleTitle = function () {
                if (!$scope.isEdit) {
                    return " Add New Category";
                }
                return " Edit Category";
            }
            $scope.ok = function (form) 
            {
                console.log();

                if(HelperService.getLength(form.$error)> 0)
                {
                   HelperService.activateTabError(form.$error);
                   return;
                }
               

                var closeDialog = function (success) {
                    $uibModalInstance.close({
                        success: success
                    });
                }
                
                $rootScope.showLoader = true;
                $scope.selectedArticle.parent_id = $scope.parentCategory.selected.id;
                if (item == null) 
                {


                    var createPromise = ArticleService.create($scope.selectedArticle);
                    createPromise.then(function success(data) {
                        $rootScope.showLoader = false;
                        alertify.success("Successfully");
                        closeDialog(true);
                    }, function error(data) {
                        $rootScope.showLoader = false;
                        alertify.error("Sorry error occured!");
                        //closeDialog(false);
                    }
                    );
                }
                else
                 {
                    debugger;
                    var updatePromise = ArticleService.update(itemId,$scope.selectedArticle);
                    updatePromise.then(function () {
                        $rootScope.showLoader = false;
                        alertify.success("Successfully");
                        closeDialog(true);
                    }, function () {
                        $rootScope.showLoader = false;
                        $scope.selectedArticle = backUpModule;
                        alertify.error("Sorry error occured!");
                        closeDialog(false);
                    });
                }


            };
            $scope.cancel = function () {
                $scope.selectedArticle = $scope.backUpModule;
                $uibModalInstance.dismiss('cancel');
            };
            

        }]);