angular.module("KhmerBlog", ["ngRoute", "ngResource", "ngTable", 'ui.bootstrap', "KhmerBlog.controllers", "KhmerBlog.services", "ngAlertify",'ui.select'])
    .factory('httpInterceptor',function(){
        return{
            request: function(config)
                    {
                        //console.log(config);
                        // if(!config.url.endsWith('.html') && config.url.indexOf('template') ==-1)
                        // {
                        //     config.url = config.url+"?XDEBUG_SESSION_START=sublime.xdebug";    
                        // }
                        
                        // console.log('http interceptors',config.url);
                        // if(!config.url.endsWith('.html'))
                        // {
                        //     config.url = "http://localhost:8888/khmerblog/public/administrator"+config.url;    
                        // }
                        
                        return config;
                    }
        } 
    })
    .config(["$routeProvider", "$httpProvider" ,function ($routeProvider,$httpProvider)
    {
        $routeProvider.when("/module", {
            templateUrl: "/Home/Module",
        });
        $routeProvider.when("/category", {
            templateUrl:"administrator/template/category",
        });
        $routeProvider.when("/media", {
            templateUrl:"administrator/template/media",
        });
        $routeProvider.otherwise({
            templateUrl :"administrator/template",
        });
        $routeProvider.when('/article',{
            templateUrl: "administrator/template/article"
        });
        
        $routeProvider.when('/article-eidt',
        {
            templateUrl: "administrator/template/view?template=editArticle"
        })
        $httpProvider.interceptors.push('httpInterceptor');
        //console.log('yes push',$httpProvider);
    }])
    .controller("KhmerBlogCtrl", ['$scope', '$http', "$rootScope", "alertify", function ($scope, $http, $rootScope,alertify) {
      "ngInject";
      alertify.logPosition("bottom right");
    $scope.selectedMenu = null;
    $scope.selectedSubMenu = null;
    $rootScope.showLoader = false;
    $rootScope.languages  = [];
    $scope.loadLanguage = function () 
    {
        //$scope.languages = { total: 0, data: [] };
        $http.get("administrator/language").success(function (data) 
        {
            $rootScope.languages = data;
            console.log(data);
        });
    };
   
    $scope.loadLanguage();
    

    

    }]);
// .run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
//     var original = $location.path;
//     angular.forEach($route.routes,function(value,key)
//     {
//         if(value.templateUrl)
//         {
//             value.templateUrl = "http://localhost:8888/khmerblog/public/administrator"+value.templateUrl;    
//         }
        
//         $route.routes[key] = value;
//     });
//     $location.path = function (path, reload) {
//         if (reload === false) {
//             var lastRoute = $route.current;
//             var un = $rootScope.$on('$locationChangeSuccess', function () {
//                 $route.current = lastRoute;
//                 un();
//             });
//         }
//         return original.apply($location, [path]);
//     };
// }]);

var KhmerBlogService = angular.module('KhmerBlog.services', ['ngResource']).value('version', '0.1');