angular.module("MyHRM")
.controller("ModuleCtrl", ["$scope", "ModuleService", "$q", "NgTableParams", "$uibModal","$rootScope",
    function ($scope, ModuleService, $q, NgTableParams, $uibModal,$rootScope)
{
    $scope.data = $scope.data || {};
    $scope.data.modules = {};
    $scope.data.moduleQuery = "";
    $scope.data.moduleTitle = "Add New Module";
    var that = this;

    var getModalInstance  = function()
    {
       return $uibModal.open({
            animation: true,
            templateUrl: "/Home/EditModule",
            controller: "ModuleModalCtrl",
            bindToController: true,
            size: "",
            resolve: {
                item: function ()
                {
                    return $scope.selectedModule;
                }
            }
        });
    }

    $scope.addModule = function () {
        $scope.selectedModule = null;
        modalInstance = getModalInstance();
        modalInstance.result.then(function (result) {
         
            if(result.success)
            {
                that.tableParams.reload();
            }
        });
    };

    $scope.editModule = function (module) {
        // "EditModule";
        $scope.selectedModule = module;
        modalInstance = getModalInstance();
        modalInstance.result.then(function (module) {
            
           // that.tableParams.reload();
        });
    };
    
    this.tableParams = new NgTableParams({}, {
        getData: function (params)
        {
          
            $rootScope.showLoader = true;
            var moduleQueryPromise = ModuleService.query({
                option: {
                    PerPage: params.count(),
                    Page: params.page(),
                    Filter: params.filter(),
                    Sorting: params.sorting(),
                }
            });
            return moduleQueryPromise.then(function (modules) {
                params.total(modules.total);
                $rootScope.showLoader = false;
                return modules.data;

            });
        }
    });
    

    //moduleQueryPromise.then(function (modules) {
    //    $scope.data.modules = modules;
    //    console.log(modules);
    //});
    $scope.finishLoading = function ()
    {
        console.log('finish loading');
    };
    //$q.all([moduleQueryPromise]).then(function (data) {
    //    $scope.finishLoading();
    //});
    
    }]).controller("ModuleModalCtrl", ["$scope", "$http", "$q",
        "$uibModalInstance", "item", "$rootScope", "ModuleService", "alertify",
        function ($scope, $http, $q, $uibModalInstance, item, $rootScope, ModuleService, alertify)
        {
            $scope.selectedModule = item;
            console.log(item);
            var backUpModule = item;
            $scope.isEdit = item !=null;
            $scope.getModuleTitle = function (){
                if (!$scope.isEdit)
                {
                    return " Add New Module";
                }
                return " Edit Module";
            }
            $scope.ok = function ()
            {
                var closeDialog = function (success) {
                    $uibModalInstance.close({
                        success: success
                    });
                }
                if ($scope.selectedModule.ParentId)
                {
                    var parent = $scope.parentDic[$scope.selectedModule.ParentId];
                    if (parent)
                    {
                        $scope.selectedModule.Level = parent.Level + 1;
                    }
                }
                $rootScope.showLoader = true;
                if (item == null)
                {
                    

                    var createPromise = ModuleService.create($scope.selectedModule);
                    createPromise.then(function success(data) {
                        $rootScope.showLoader = false;
                        alertify.success("Successfully");
                        closeDialog(true);
                    }, function error(data) {
                        $rootScope.showLoader = false;
                        alertify.error("Sorry error occured!");
                        closeDialog(false);
                    }
                    );
                }
                else
                {
                    var updatePromise = ModuleService.update($scope.selectedModule);
                    updatePromise.then(function (){
                        $rootScope.showLoader = false;
                        alertify.success("Successfully");
                        closeDialog(true);
                    }, function ()
                    {
                        $rootScope.showLoader = false;
                        $scope.selectedModule = backUpModule;
                        alertify.error("Sorry error occured!");
                        closeDialog(false);
                    });
                }

               
            };
            $scope.cancel = function ()
            {
                $uibModalInstance.dismiss('cancel');
            };
            this.getListParent = function (item)
            {
                var defer = $q.defer();
                var id = 0;
                if (item) {
                    id = item.id;
                }
                $http.get("/api/modules/tree?except=" + id)
                    .success(function (data) {
                        defer.resolve(data);
                    }).error(function(data){
                        defer.reject(data);
                    });
                return defer.promise;

            };
            var parentPromise = this.getListParent(item);
            var getLevelStr = function (level) {
                console.log("Level", level);
                var str = "";
                for(var i =0;i<level;i++)
                {
                    str += "--";
                }
                return str;
            };
            parentPromise.then(function (data)
            {
                $scope.parentModules = [];
                $scope.parentDic = {};
                var data = data.data;
                
                for (var key in data)
                {
                    var obj = data[key];
                    $scope.parentModules.push(obj);
                    obj.Name = getLevelStr(obj.Level) + obj.Name;
                    $scope.parentDic[obj.Id] = obj;
                    if(obj.SubModules)

                    {
                        for(var sub in obj.SubModules)
                        {
                            var tmp = obj.SubModules[sub];
                            tmp.Name = getLevelStr(tmp.Level)+ tmp.Name;
                            $scope.parentModules.push(tmp);
                            $scope.parentDic[tmp.Id] = tmp;
                        }
                    }

                }
              
            });

            $q.all([parentPromise]).then(function (data) {
                console.log('finish loading');
            });

        }]);