$(document).ready(function() {
    	$("body").fadeIn(400);
		$('#myCarousel').carousel()
		$('#newProductCar').carousel()

		/* Home page item price animation */
		$('.product-thumbnail').mouseenter(function() {
		   $(this).children('.zoomTool').fadeIn();
		});

		$('.product-thumbnail').mouseleave(function() {
			$(this).children('.zoomTool').fadeOut();
		});
		$('.face-list').click(function(){
			if($('.subnav-list').hasClass('active')){
				$('.subnav-list').fadeOut(200);
				$('.subnav-list').removeClass('active');
			}
			else {
				$('.subnav-list').fadeIn(200);
				$('.subnav-list').addClass('active');
			}
		});
		//$('.nav .face-list').click(function() {
		//	if($('.subnav-list').hasClass('active')){
		//		console.log("ffff");
		//	}
		//	$('.nav .subnav-list').addClass('active');
		//	$('.subnav-list').fadeIn(200);
		//});

		// Show/Hide Sticky "Go to top" button
			$(window).scroll(function(){
				if($(this).scrollTop()>200){
					$(".gotop").fadeIn(200);
				}
				else{
					$(".gotop").fadeOut(200);
				}
			});
		// Scroll Page to Top when clicked on "go to top" button
			$(".gotop").click(function(event){
				event.preventDefault();

				$.scrollTo('#gototop', 1500, {
					easing: 'easeOutCubic'
				});
			});

});
