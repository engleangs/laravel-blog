# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: db_blog
# Generation Time: 2016-10-29 16:54:27 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `lang_id` int(11) NOT NULL DEFAULT '0',
  `is_delete` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `post_num` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `description`, `lang_id`, `is_delete`, `created_at`, `updated_at`, `created_by`, `updated_by`, `post_num`, `lft`, `rgt`, `level`, `parent_id`)
VALUES
	(1,'ROOT',NULL,1,0,NULL,NULL,NULL,NULL,0,1,6,0,NULL),
	(1,'ROOT',NULL,2,0,NULL,NULL,NULL,NULL,0,1,6,0,NULL),
	(4,'C1','',1,1,'2016-10-29 14:37:21','2016-10-29 14:37:21',NULL,NULL,0,2,1,1,1),
	(4,'C1','',2,1,'2016-10-29 14:37:21','2016-10-29 14:37:21',NULL,NULL,0,2,1,1,1),
	(5,'C2','C2',1,1,'2016-10-29 14:37:32','2016-10-29 14:37:32',NULL,NULL,0,3,4,2,4),
	(5,'C2','',2,1,'2016-10-29 14:37:32','2016-10-29 14:37:32',NULL,NULL,0,3,4,2,4),
	(6,'C3','',1,0,'2016-10-29 14:37:42','2016-10-29 14:37:42',NULL,NULL,0,2,5,1,1),
	(6,'C3','',2,0,'2016-10-29 14:37:42','2016-10-29 14:37:42',NULL,NULL,0,2,5,1,1),
	(7,'C4','',1,0,'2016-10-29 14:37:54','2016-10-29 14:37:54',NULL,NULL,0,3,4,2,6),
	(7,'C4','',2,0,'2016-10-29 14:37:54','2016-10-29 14:37:54',NULL,NULL,0,3,4,2,6);

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table languages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `flag` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;

INSERT INTO `languages` (`id`, `name`, `description`, `flag`)
VALUES
	(1,'EN','English',NULL),
	(2,'KH','Khmer',NULL);

/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `tbl` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `ref` varchar(30) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;

INSERT INTO `logs` (`id`, `user_name`, `created_at`, `updated_at`, `action`, `tbl`, `description`, `ref`, `data`)
VALUES
	(1,'','2016-03-29 19:32:30','2016-03-29 19:32:30','insert','categories','add new category','4','{\"name\":\"dd\",\"description\":\"dd\",\"id\":4}'),
	(2,'','2016-03-29 19:45:48','2016-03-29 19:45:48','insert','categories','add new category','5','{\"name\":\"ddd\",\"description\":\"ddd\",\"id\":5}'),
	(3,'','2016-03-29 19:47:41','2016-03-29 19:47:41','insert','categories','add new category','6','{\"name\":\"eee\",\"description\":\"eee\",\"id\":6}'),
	(4,'','2016-03-29 19:53:34','2016-03-29 19:53:34','insert','categories','add new category','7','{\"name\":\"ddd\",\"description\":\"d\",\"id\":7}'),
	(5,'','2016-03-29 19:55:21','2016-03-29 19:55:21','insert','categories','add new category','8','{\"description\":\"ddd\",\"name\":\"ddd\",\"id\":8}'),
	(6,'','2016-03-29 19:56:39','2016-03-29 19:56:39','insert','categories','add new category','9','{\"name\":\"ddd1\",\"description\":\"dfdffddf\",\"id\":9}'),
	(7,'','2016-04-02 19:21:31','2016-04-02 19:21:31','insert','categories','add new category','10','{\"name\":\"Hello\",\"description\":\"ddd\",\"id\":10}'),
	(8,'','2016-04-03 19:16:36','2016-04-03 19:16:36','insert','categories','add new category','12','{\"name\":\"ddd\",\"description\":\"ddd\",\"id\":12}'),
	(9,'','2016-04-03 19:18:31','2016-04-03 19:18:31','insert','categories','add new category','13','{\"name\":\"Hello\",\"description\":\"dddd\",\"id\":13}'),
	(10,'','2016-04-03 19:35:23','2016-04-03 19:35:23','insert','categories','add new category','14','{\"name\":\"ddd\",\"description\":\"ddd\",\"id\":14}'),
	(11,'','2016-04-03 19:35:58','2016-04-03 19:35:58','insert','categories','add new category','15','{\"name\":\"dddd1\",\"description\":\"dddd1\",\"id\":15}'),
	(12,'','2016-04-03 19:37:46','2016-04-03 19:37:46','insert','categories','add new category','16','{\"name\":\"hello\",\"description\":\"hello\",\"id\":16}'),
	(13,'','2016-04-03 19:38:26','2016-04-03 19:38:26','insert','categories','add new category','17','{\"name\":\"l2\",\"description\":\"l2\",\"id\":17}'),
	(14,'','2016-04-03 19:43:11','2016-04-03 19:43:11','insert','categories','add new category','18','{\"name\":\"l3\",\"description\":\"l3\",\"id\":18}'),
	(15,'','2016-04-03 20:10:39','2016-04-03 20:10:39','insert','categories','add new category','19','{\"name\":\"l1\",\"description\":\"l1\",\"id\":19}'),
	(16,'','2016-04-03 20:11:52','2016-04-03 20:11:52','insert','categories','add new category','20','{\"name\":\"a1\",\"description\":\"a1\",\"id\":20}'),
	(17,'','2016-04-03 20:12:40','2016-04-03 20:12:40','delete','categories','delete category','20','{\"id\":20,\"name\":\"a1\",\"description\":\"a1\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-04-03 20:11:52\",\"updated_at\":\"2016-04-03 20:11:52\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":3,\"rgt\":4,\"level\":2,\"parent_id\":19}'),
	(18,'','2016-04-03 20:14:14','2016-04-03 20:14:14','insert','categories','add new category','21','{\"name\":\"l2\",\"description\":\"l2\",\"id\":21}'),
	(19,'','2016-04-03 20:20:41','2016-04-03 20:20:41','insert','categories','add new category','22','{\"name\":\"l2\",\"description\":\"l2\",\"id\":22}'),
	(20,'','2016-04-03 20:20:58','2016-04-03 20:20:58','insert','categories','add new category','23','{\"name\":\"l3\",\"description\":\"l3\",\"id\":23}'),
	(21,'','2016-04-03 20:22:53','2016-04-03 20:22:53','delete','categories','delete category','23','{\"id\":23,\"name\":\"l3\",\"description\":\"l3\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-04-03 20:20:58\",\"updated_at\":\"2016-04-03 20:20:58\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":3,\"rgt\":4,\"level\":2,\"parent_id\":22}'),
	(22,'','2016-04-03 20:27:04','2016-04-03 20:27:04','insert','categories','add new category','24','{\"name\":\"l3\",\"description\":\"l3\",\"id\":24}'),
	(23,'','2016-04-03 20:27:46','2016-04-03 20:27:46','insert','categories','add new category','25','{\"name\":\"a2\",\"description\":\"a2\",\"id\":25}'),
	(24,'','2016-04-03 20:28:05','2016-04-03 20:28:05','insert','categories','add new category','26','{\"name\":\"a3\",\"description\":\"a3\",\"id\":26}'),
	(25,'','2016-04-03 20:28:28','2016-04-03 20:28:28','delete','categories','delete category','22','{\"id\":22,\"name\":\"l2\",\"description\":\"l2\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-04-03 20:20:41\",\"updated_at\":\"2016-04-03 20:20:41\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":5,\"level\":1,\"parent_id\":1}'),
	(26,'','2016-06-16 19:00:21','2016-06-16 19:00:21','delete','categories','delete category','26','{\"id\":26,\"name\":\"a3\",\"description\":\"a3\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-04-03 20:28:05\",\"updated_at\":\"2016-04-03 20:28:05\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":7,\"rgt\":8,\"level\":2,\"parent_id\":25}'),
	(27,'','2016-06-16 19:00:42','2016-06-16 19:00:42','delete','categories','delete category','25','{\"id\":25,\"name\":\"a2\",\"description\":\"a2\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-04-03 20:27:46\",\"updated_at\":\"2016-04-03 20:27:46\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":6,\"rgt\":7,\"level\":1,\"parent_id\":1}'),
	(28,'','2016-06-16 19:01:07','2016-06-16 19:01:07','delete','categories','delete category','24','{\"id\":24,\"name\":\"l3\",\"description\":\"l3\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-04-03 20:27:04\",\"updated_at\":\"2016-04-03 20:27:04\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":3,\"rgt\":4,\"level\":2,\"parent_id\":22}'),
	(29,'','2016-06-16 19:01:37','2016-06-16 19:01:37','insert','categories','add new category','27','{\"name\":\"Technology\",\"description\":\"dddd\",\"id\":27}'),
	(30,'','2016-06-16 19:15:22','2016-06-16 19:15:22','insert','categories','add new category','28','{\"name\":\"Literature\",\"description\":\"testing\",\"id\":28}'),
	(31,'','2016-06-16 19:15:46','2016-06-16 19:15:46','insert','categories','add new category','29','{\"name\":\"Engineer\",\"description\":\"Engineer\",\"id\":29}'),
	(32,'','2016-06-16 19:16:03','2016-06-16 19:16:03','insert','categories','add new category','30','{\"name\":\"Math\",\"description\":\"Math\",\"id\":30}'),
	(33,'','2016-06-16 19:16:20','2016-06-16 19:16:20','insert','categories','add new category','31','{\"name\":\"Calculus\",\"description\":\"Calculus\",\"id\":31}'),
	(34,'','2016-06-16 19:16:38','2016-06-16 19:16:38','delete','categories','delete category','30','{\"id\":30,\"name\":\"Math\",\"description\":\"Math\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 19:16:03\",\"updated_at\":\"2016-06-16 19:16:03\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":7,\"rgt\":10,\"level\":2,\"parent_id\":27}'),
	(35,'','2016-06-16 19:19:36','2016-06-16 19:19:36','insert','categories','add new category','2','{\"name\":\"Math\",\"description\":\"Math\",\"id\":2}'),
	(36,'','2016-06-16 19:19:50','2016-06-16 19:19:50','insert','categories','add new category','3','{\"name\":\"Math\",\"description\":\"Math\",\"id\":3}'),
	(37,'','2016-06-16 19:22:51','2016-06-16 19:22:51','insert','categories','add new category','2','{\"name\":\"Math\",\"description\":\"Math\",\"id\":2}'),
	(38,'','2016-06-16 19:23:10','2016-06-16 19:23:10','insert','categories','add new category','3','{\"name\":\"Calculus\",\"id\":3}'),
	(39,'','2016-06-16 19:23:11','2016-06-16 19:23:11','insert','categories','add new category','4','{\"name\":\"Calculus\",\"id\":4}'),
	(40,'','2016-06-16 19:23:12','2016-06-16 19:23:12','insert','categories','add new category','5','{\"name\":\"Calculus\",\"id\":5}'),
	(41,'','2016-06-16 19:23:13','2016-06-16 19:23:13','insert','categories','add new category','6','{\"name\":\"Calculus\",\"id\":6}'),
	(42,'','2016-06-16 19:23:14','2016-06-16 19:23:14','insert','categories','add new category','7','{\"name\":\"Calculus\",\"id\":7}'),
	(43,'','2016-06-16 19:23:14','2016-06-16 19:23:14','insert','categories','add new category','8','{\"name\":\"Calculus\",\"id\":8}'),
	(44,'','2016-06-16 19:23:14','2016-06-16 19:23:14','insert','categories','add new category','9','{\"name\":\"Calculus\",\"id\":9}'),
	(45,'','2016-06-16 19:23:15','2016-06-16 19:23:15','insert','categories','add new category','10','{\"name\":\"Calculus\",\"id\":10}'),
	(46,'','2016-06-16 19:24:13','2016-06-16 19:24:13','insert','categories','add new category','11','{\"name\":\"Calculus\",\"id\":11}'),
	(47,'','2016-06-16 19:25:37','2016-06-16 19:25:37','insert','categories','add new category','2','{\"name\":\"Math\",\"id\":2}'),
	(48,'','2016-06-16 19:25:56','2016-06-16 19:25:56','insert','categories','add new category','3','{\"name\":\"Physic\",\"description\":\"Physic\",\"id\":3}'),
	(49,'','2016-06-16 19:26:15','2016-06-16 19:26:15','insert','categories','add new category','4','{\"name\":\"Caculus\",\"id\":4}'),
	(50,'','2016-06-16 19:47:06','2016-06-16 19:47:06','delete','categories','delete category','2','{\"id\":2,\"name\":\"Math\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 19:25:37\",\"updated_at\":\"2016-06-16 19:25:37\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":36,\"rgt\":39,\"level\":1,\"parent_id\":1}'),
	(51,'','2016-06-16 19:49:06','2016-06-16 19:49:06','insert','categories','add new category','5','{\"name\":\"Math\",\"id\":5}'),
	(52,'','2016-06-16 19:49:25','2016-06-16 19:49:25','insert','categories','add new category','6','{\"name\":\"Technology\",\"id\":6}'),
	(53,'','2016-06-16 19:49:42','2016-06-16 19:49:42','insert','categories','add new category','7','{\"name\":\"b1\",\"description\":\"b1\",\"id\":7}'),
	(54,'','2016-06-16 19:50:19','2016-06-16 19:50:19','delete','categories','delete category','5','{\"id\":5,\"name\":\"Math\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 19:49:06\",\"updated_at\":\"2016-06-16 19:49:06\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":40,\"rgt\":45,\"level\":1,\"parent_id\":1}'),
	(55,'','2016-06-16 19:53:20','2016-06-16 19:53:20','insert','categories','add new category','2','{\"name\":\"Math\",\"id\":2}'),
	(56,'','2016-06-16 19:54:11','2016-06-16 19:54:11','insert','categories','add new category','3','{\"name\":\"Technology\",\"id\":3}'),
	(57,'','2016-06-16 19:54:28','2016-06-16 19:54:28','insert','categories','add new category','4','{\"name\":\"B1\",\"id\":4}'),
	(58,'','2016-06-16 20:00:39','2016-06-16 20:00:39','delete','categories','delete category','2','{\"id\":2,\"name\":\"Math\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 19:53:20\",\"updated_at\":\"2016-06-16 19:53:20\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":7,\"level\":1,\"parent_id\":1}'),
	(59,'','2016-06-16 20:02:05','2016-06-16 20:02:05','insert','categories','add new category','5','{\"name\":\"A\",\"description\":\"A\",\"id\":5}'),
	(60,'','2016-06-16 20:02:22','2016-06-16 20:02:22','insert','categories','add new category','6','{\"name\":\"B\",\"description\":\"B\",\"id\":6}'),
	(61,'','2016-06-16 20:02:38','2016-06-16 20:02:38','insert','categories','add new category','7','{\"name\":\"C\",\"description\":\"C\",\"id\":7}'),
	(62,'','2016-06-16 20:02:57','2016-06-16 20:02:57','insert','categories','add new category','8','{\"name\":\"D\",\"description\":\"D\",\"id\":8}'),
	(63,'','2016-06-16 20:03:07','2016-06-16 20:03:07','delete','categories','delete category','5','{\"id\":5,\"name\":\"A\",\"description\":\"A\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:02:05\",\"updated_at\":\"2016-06-16 20:02:05\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":6,\"rgt\":11,\"level\":1,\"parent_id\":1}'),
	(64,'','2016-06-16 20:03:20','2016-06-16 20:03:20','insert','categories','add new category','9','{\"name\":\"E\",\"description\":\"E\",\"id\":9}'),
	(65,'','2016-06-16 20:03:42','2016-06-16 20:03:42','insert','categories','add new category','10','{\"name\":\"F\",\"description\":\"F\",\"id\":10}'),
	(66,'','2016-06-16 20:04:03','2016-06-16 20:04:03','insert','categories','add new category','11','{\"name\":\"R\",\"description\":\"R\",\"id\":11}'),
	(67,'','2016-06-16 20:04:19','2016-06-16 20:04:19','insert','categories','add new category','12','{\"name\":\"N\",\"description\":\"N\",\"id\":12}'),
	(68,'','2016-06-16 20:04:32','2016-06-16 20:04:32','insert','categories','add new category','13','{\"name\":\"M\",\"description\":\"M\",\"id\":13}'),
	(69,'','2016-06-16 20:04:36','2016-06-16 20:04:36','delete','categories','delete category','11','{\"id\":11,\"name\":\"R\",\"description\":\"R\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:04:03\",\"updated_at\":\"2016-06-16 20:04:03\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":17,\"rgt\":20,\"level\":2,\"parent_id\":8}'),
	(70,'','2016-06-16 20:04:43','2016-06-16 20:04:43','delete','categories','delete category','8','{\"id\":8,\"name\":\"D\",\"description\":\"D\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:02:57\",\"updated_at\":\"2016-06-16 20:02:57\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":12,\"rgt\":21,\"level\":1,\"parent_id\":1}'),
	(71,'','2016-06-16 20:07:10','2016-06-16 20:07:10','insert','categories','add new category','2','{\"name\":\"A\",\"description\":\"A\",\"id\":2}'),
	(72,'','2016-06-16 20:08:00','2016-06-16 20:08:00','insert','categories','add new category','3','{\"name\":\"A\",\"description\":\"A\",\"id\":3}'),
	(73,'','2016-06-16 20:08:12','2016-06-16 20:08:12','delete','categories','delete category','3','{\"id\":3,\"name\":\"A\",\"description\":\"A\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:08:00\",\"updated_at\":\"2016-06-16 20:08:00\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":3,\"level\":1,\"parent_id\":1}'),
	(74,'','2016-06-16 20:09:00','2016-06-16 20:09:00','insert','categories','add new category','4','{\"name\":\"Math\",\"id\":4}'),
	(75,'','2016-06-16 20:09:22','2016-06-16 20:09:22','insert','categories','add new category','5','{\"name\":\"B\",\"description\":\"B\",\"id\":5}'),
	(76,'','2016-06-16 20:09:46','2016-06-16 20:09:46','insert','categories','add new category','6','{\"name\":\"C\",\"description\":\"C\",\"id\":6}'),
	(77,'','2016-06-16 20:10:05','2016-06-16 20:10:05','delete','categories','delete category','4','{\"id\":4,\"name\":\"Math\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:09:00\",\"updated_at\":\"2016-06-16 20:09:00\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":7,\"level\":1,\"parent_id\":1}'),
	(78,'','2016-06-16 20:11:36','2016-06-16 20:11:36','insert','categories','add new category','7','{\"name\":\"A\",\"description\":\"A\",\"id\":7}'),
	(79,'','2016-06-16 20:11:47','2016-06-16 20:11:47','insert','categories','add new category','8','{\"name\":\"B\",\"description\":\"B\",\"id\":8}'),
	(80,'','2016-06-16 20:12:14','2016-06-16 20:12:14','insert','categories','add new category','9','{\"name\":\"C\",\"description\":\"C\",\"id\":9}'),
	(81,'','2016-06-16 20:12:33','2016-06-16 20:12:33','insert','categories','add new category','10','{\"name\":\"b1\",\"id\":10}'),
	(82,'','2016-06-16 20:12:47','2016-06-16 20:12:47','insert','categories','add new category','11','{\"name\":\"b2\",\"id\":11}'),
	(83,'','2016-06-16 20:14:02','2016-06-16 20:14:02','delete','categories','delete category','8','{\"id\":8,\"name\":\"B\",\"description\":\"B\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:11:47\",\"updated_at\":\"2016-06-16 20:11:47\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":3,\"rgt\":8,\"level\":2,\"parent_id\":7}'),
	(84,'','2016-06-16 20:23:32','2016-06-16 20:23:32','insert','categories','add new category','12','{\"name\":\"A\",\"description\":\"a\",\"id\":12}'),
	(85,'','2016-06-16 20:23:41','2016-06-16 20:23:41','insert','categories','add new category','13','{\"name\":\"B\",\"id\":13}'),
	(86,'','2016-06-16 20:23:50','2016-06-16 20:23:50','insert','categories','add new category','14','{\"name\":\"C\",\"description\":\"C\",\"id\":14}'),
	(87,'','2016-06-16 20:24:00','2016-06-16 20:24:00','insert','categories','add new category','15','{\"name\":\"B1\",\"id\":15}'),
	(88,'','2016-06-16 20:24:20','2016-06-16 20:24:20','insert','categories','add new category','16','{\"name\":\"C1\",\"description\":\"\",\"id\":16}'),
	(89,'','2016-06-16 20:24:41','2016-06-16 20:24:41','insert','categories','add new category','17','{\"name\":\"B2\",\"description\":\"B2\",\"id\":17}'),
	(90,'','2016-06-16 20:26:40','2016-06-16 20:26:40','insert','categories','add new category','18','{\"name\":\"A\",\"description\":\"A\",\"id\":18}'),
	(91,'','2016-06-16 20:26:53','2016-06-16 20:26:53','insert','categories','add new category','19','{\"name\":\"B\",\"description\":\"B\",\"id\":19}'),
	(92,'','2016-06-16 20:27:04','2016-06-16 20:27:04','insert','categories','add new category','20','{\"name\":\"C\",\"id\":20}'),
	(93,'','2016-06-16 20:27:22','2016-06-16 20:27:22','insert','categories','add new category','21','{\"name\":\"D\",\"description\":\"D\",\"id\":21}'),
	(94,'','2016-06-16 20:28:02','2016-06-16 20:28:02','delete','categories','delete category','19','{\"id\":19,\"name\":\"B\",\"description\":\"B\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:26:53\",\"updated_at\":\"2016-06-16 20:26:53\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":3,\"rgt\":8,\"level\":2,\"parent_id\":18}'),
	(95,'','2016-06-16 20:30:26','2016-06-16 20:30:26','insert','categories','add new category','22','{\"name\":\"A\",\"description\":\"A\",\"id\":22}'),
	(96,'','2016-06-16 20:30:38','2016-06-16 20:30:38','insert','categories','add new category','23','{\"name\":\"B\",\"description\":\"B\",\"id\":23}'),
	(97,'','2016-06-16 20:30:50','2016-06-16 20:30:50','insert','categories','add new category','24','{\"name\":\"C\",\"description\":\"C\",\"id\":24}'),
	(98,'','2016-06-16 20:31:38','2016-06-16 20:31:38','delete','categories','delete category','23','{\"id\":23,\"name\":\"B\",\"description\":\"B\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:30:38\",\"updated_at\":\"2016-06-16 20:30:38\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":3,\"rgt\":6,\"level\":2,\"parent_id\":22}'),
	(99,'','2016-06-16 20:36:18','2016-06-16 20:36:18','insert','categories','add new category','25','{\"name\":\"A\",\"id\":25}'),
	(100,'','2016-06-16 20:36:30','2016-06-16 20:36:30','insert','categories','add new category','26','{\"name\":\"B\",\"description\":\"B\",\"id\":26}'),
	(101,'','2016-06-16 20:36:44','2016-06-16 20:36:44','delete','categories','delete category','25','{\"id\":25,\"name\":\"A\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:36:18\",\"updated_at\":\"2016-06-16 20:36:18\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":5,\"level\":1,\"parent_id\":1}'),
	(102,'','2016-06-16 20:38:33','2016-06-16 20:38:33','insert','categories','add new category','27','{\"name\":\"A\",\"description\":\"A\",\"id\":27}'),
	(103,'','2016-06-16 20:38:45','2016-06-16 20:38:45','insert','categories','add new category','28','{\"name\":\"A1\",\"description\":\"A1\",\"id\":28}'),
	(104,'','2016-06-16 20:39:03','2016-06-16 20:39:03','insert','categories','add new category','29','{\"name\":\"A2\",\"description\":\"A2\",\"id\":29}'),
	(105,'','2016-06-16 20:39:15','2016-06-16 20:39:15','insert','categories','add new category','30','{\"name\":\"B\",\"id\":30}'),
	(106,'','2016-06-16 20:39:51','2016-06-16 20:39:51','delete','categories','delete category','28','{\"id\":28,\"name\":\"A1\",\"description\":\"A1\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:38:45\",\"updated_at\":\"2016-06-16 20:38:45\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":3,\"rgt\":6,\"level\":2,\"parent_id\":27}'),
	(107,'','2016-06-16 20:40:18','2016-06-16 20:40:18','insert','categories','add new category','31','{\"name\":\"B1\",\"id\":31}'),
	(108,'','2016-06-16 20:40:28','2016-06-16 20:40:28','insert','categories','add new category','32','{\"name\":\"B2\",\"id\":32}'),
	(109,'','2016-06-16 20:40:45','2016-06-16 20:40:45','insert','categories','add new category','33','{\"name\":\"B21\",\"description\":\"\",\"id\":33}'),
	(110,'','2016-06-16 20:41:27','2016-06-16 20:41:27','delete','categories','delete category','27','{\"id\":27,\"name\":\"A\",\"description\":\"A\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:38:33\",\"updated_at\":\"2016-06-16 20:38:33\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":3,\"level\":1,\"parent_id\":1}'),
	(111,'','2016-06-16 20:43:06','2016-06-16 20:43:06','delete','categories','delete category','27','{\"id\":27,\"name\":\"A\",\"description\":\"A\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:38:33\",\"updated_at\":\"2016-06-16 20:38:33\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":3,\"level\":1,\"parent_id\":1}'),
	(112,'','2016-06-16 20:44:48','2016-06-16 20:44:48','delete','categories','delete category','27','{\"id\":27,\"name\":\"A\",\"description\":\"A\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:38:33\",\"updated_at\":\"2016-06-16 20:38:33\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":3,\"level\":1,\"parent_id\":1}'),
	(113,'','2016-06-16 20:46:55','2016-06-16 20:46:55','insert','categories','add new category','34','{\"name\":\"C\",\"id\":34}'),
	(114,'','2016-06-16 20:47:06','2016-06-16 20:47:06','insert','categories','add new category','35','{\"name\":\"D\",\"description\":\"D\",\"id\":35}'),
	(115,'','2016-06-16 20:47:13','2016-06-16 20:47:13','delete','categories','delete category','30','{\"id\":30,\"name\":\"B\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:39:15\",\"updated_at\":\"2016-06-16 20:39:15\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":9,\"level\":1,\"parent_id\":1}'),
	(116,'','2016-06-16 20:47:40','2016-06-16 20:47:40','insert','categories','add new category','36','{\"name\":\"E\",\"id\":36}'),
	(117,'','2016-06-16 20:47:44','2016-06-16 20:47:44','delete','categories','delete category','34','{\"id\":34,\"name\":\"C\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:46:55\",\"updated_at\":\"2016-06-16 20:46:55\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":5,\"level\":1,\"parent_id\":1}'),
	(118,'','2016-06-16 20:48:08','2016-06-16 20:48:08','insert','categories','add new category','37','{\"name\":\"B\",\"description\":\"B\",\"id\":37}'),
	(119,'','2016-06-16 20:48:16','2016-06-16 20:48:16','insert','categories','add new category','38','{\"name\":\"C\",\"id\":38}'),
	(120,'','2016-06-16 20:48:29','2016-06-16 20:48:29','insert','categories','add new category','39','{\"name\":\"E1\",\"id\":39}'),
	(121,'','2016-06-16 20:48:48','2016-06-16 20:48:48','delete','categories','delete category','36','{\"id\":36,\"name\":\"E\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:47:40\",\"updated_at\":\"2016-06-16 20:47:40\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":5,\"level\":1,\"parent_id\":1}'),
	(122,'','2016-06-16 20:49:23','2016-06-16 20:49:23','insert','categories','add new category','40','{\"name\":\"F\",\"description\":\"F\",\"id\":40}'),
	(123,'','2016-06-16 20:49:33','2016-06-16 20:49:33','insert','categories','add new category','41','{\"name\":\"G\",\"description\":\"G\",\"id\":41}'),
	(124,'','2016-06-16 20:49:49','2016-06-16 20:49:49','delete','categories','delete category','40','{\"id\":40,\"name\":\"F\",\"description\":\"F\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-06-16 20:49:23\",\"updated_at\":\"2016-06-16 20:49:23\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":3,\"rgt\":6,\"level\":2,\"parent_id\":37}'),
	(125,'','2016-10-29 14:12:07','2016-10-29 14:12:07','insert','categories','add new category','2','{\"name\":\"test\",\"description\":\"1\",\"id\":2}'),
	(126,'','2016-10-29 14:12:30','2016-10-29 14:12:30','insert','categories','add new category','3','{\"name\":\"testing2\",\"description\":\"testing2\",\"id\":3}'),
	(127,'','2016-10-29 14:12:48','2016-10-29 14:12:48','insert','categories','add new category','4','{\"name\":\"testing3\",\"description\":\"testing3\",\"id\":4}'),
	(128,'','2016-10-29 14:12:52','2016-10-29 14:12:52','delete','categories','delete category','4','{\"id\":4,\"name\":\"testing3\",\"description\":\"testing3\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-10-29 14:12:48\",\"updated_at\":\"2016-10-29 14:12:48\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":4,\"rgt\":5,\"level\":2,\"parent_id\":2}'),
	(129,'','2016-10-29 14:17:02','2016-10-29 14:17:02','insert','categories','add new category','2','{\"name\":\"Test\",\"id\":2}'),
	(130,'','2016-10-29 14:17:10','2016-10-29 14:17:10','insert','categories','add new category','3','{\"name\":\"test2\",\"id\":3}'),
	(131,'','2016-10-29 14:18:40','2016-10-29 14:18:40','delete','categories','delete category','2','{\"id\":2,\"name\":\"Test\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-10-29 14:17:02\",\"updated_at\":\"2016-10-29 14:17:02\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":5,\"level\":1,\"parent_id\":1}'),
	(132,'','2016-10-29 14:18:49','2016-10-29 14:18:49','delete','categories','delete category','2','{\"id\":2,\"name\":\"Test\",\"description\":\"\",\"lang_id\":1,\"is_delete\":1,\"created_at\":\"2016-10-29 14:17:02\",\"updated_at\":\"2016-10-29 14:17:02\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":1,\"level\":1,\"parent_id\":1}'),
	(133,'','2016-10-29 14:20:01','2016-10-29 14:20:01','insert','categories','add new category','4','{\"name\":\"test\",\"id\":4}'),
	(134,'','2016-10-29 14:21:26','2016-10-29 14:21:26','insert','categories','add new category','5','{\"name\":\"t2\",\"id\":5}'),
	(135,'','2016-10-29 14:24:18','2016-10-29 14:24:18','delete','categories','delete category','4','{\"id\":4,\"name\":\"test\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-10-29 14:20:01\",\"updated_at\":\"2016-10-29 14:20:01\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":5,\"level\":1,\"parent_id\":1}'),
	(136,'','2016-10-29 14:24:25','2016-10-29 14:24:25','delete','categories','delete category','4','{\"id\":4,\"name\":\"test\",\"description\":\"\",\"lang_id\":1,\"is_delete\":1,\"created_at\":\"2016-10-29 14:20:01\",\"updated_at\":\"2016-10-29 14:20:01\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":5,\"level\":1,\"parent_id\":1}'),
	(137,'','2016-10-29 14:26:25','2016-10-29 14:26:25','delete','categories','delete category','4','{\"id\":4,\"name\":\"test\",\"description\":\"\",\"lang_id\":1,\"is_delete\":1,\"created_at\":\"2016-10-29 14:20:01\",\"updated_at\":\"2016-10-29 14:20:01\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":5,\"level\":1,\"parent_id\":1}'),
	(138,'','2016-10-29 14:29:21','2016-10-29 14:29:21','insert','categories','add new category','6','{\"name\":\"testing\",\"id\":6}'),
	(139,'','2016-10-29 14:29:31','2016-10-29 14:29:31','insert','categories','add new category','7','{\"name\":\"a2\",\"id\":7}'),
	(140,'','2016-10-29 14:29:40','2016-10-29 14:29:40','insert','categories','add new category','8','{\"name\":\"a3\",\"id\":8}'),
	(141,'','2016-10-29 14:30:16','2016-10-29 14:30:16','insert','categories','add new category','9','{\"name\":\"a4\",\"id\":9}'),
	(142,'','2016-10-29 14:30:26','2016-10-29 14:30:26','insert','categories','add new category','10','{\"name\":\"a5\",\"id\":10}'),
	(143,'','2016-10-29 14:31:05','2016-10-29 14:31:05','delete','categories','delete category','9','{\"id\":9,\"name\":\"a4\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-10-29 14:30:15\",\"updated_at\":\"2016-10-29 14:30:15\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":8,\"rgt\":11,\"level\":1,\"parent_id\":1}'),
	(144,'','2016-10-29 14:36:26','2016-10-29 14:36:26','insert','categories','add new category','2','{\"name\":\"c1\",\"id\":2}'),
	(145,'','2016-10-29 14:36:37','2016-10-29 14:36:37','insert','categories','add new category','3','{\"name\":\"C2\",\"description\":\"C2\",\"id\":3}'),
	(146,'','2016-10-29 14:36:46','2016-10-29 14:36:46','delete','categories','delete category','2','{\"id\":2,\"name\":\"c1\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-10-29 14:36:26\",\"updated_at\":\"2016-10-29 14:36:26\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":5,\"level\":1,\"parent_id\":1}'),
	(147,'','2016-10-29 14:37:21','2016-10-29 14:37:21','insert','categories','add new category','4','{\"name\":\"C1\",\"description\":\"\",\"id\":4}'),
	(148,'','2016-10-29 14:37:32','2016-10-29 14:37:32','insert','categories','add new category','5','{\"name\":\"C2\",\"description\":\"C2\",\"id\":5}'),
	(149,'','2016-10-29 14:37:42','2016-10-29 14:37:42','insert','categories','add new category','6','{\"name\":\"C3\",\"id\":6}'),
	(150,'','2016-10-29 14:37:54','2016-10-29 14:37:54','insert','categories','add new category','7','{\"name\":\"C4\",\"id\":7}'),
	(151,'','2016-10-29 14:38:28','2016-10-29 14:38:28','delete','categories','delete category','4','{\"id\":4,\"name\":\"C1\",\"description\":\"\",\"lang_id\":1,\"is_delete\":0,\"created_at\":\"2016-10-29 14:37:21\",\"updated_at\":\"2016-10-29 14:37:21\",\"created_by\":null,\"updated_by\":null,\"post_num\":0,\"lft\":2,\"rgt\":5,\"level\":1,\"parent_id\":1}');

/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2014_10_12_000000_create_users_table',1),
	('2014_10_12_100000_create_password_resets_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table post_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `post_categories`;

CREATE TABLE `post_categories` (
  `post_id` int(11) unsigned NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table post_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `post_tags`;

CREATE TABLE `post_tags` (
  `post_id` int(11) unsigned NOT NULL,
  `tag_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`post_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) CHARACTER SET utf8 NOT NULL,
  `state` int(11) NOT NULL COMMENT '0,draft,1 publish, -1 delete',
  `content` longtext CHARACTER SET utf8 NOT NULL,
  `publish_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `feature_image` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `title`, `state`, `content`, `publish_at`, `created_at`, `updated_at`, `created_by`, `updated_by`, `lang_id`, `feature_image`)
VALUES
	(1,'Hello',0,'Hello\n','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,1,1,'1');

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(400) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
