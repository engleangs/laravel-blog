<?php
namespace Khmerblog\Models;
use Khmerblog\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use DB,Log;
abstract class BaseModel extends Model implements IRestModel
{
	protected $guarded = [];
	function getCleanData($data)
    {
        $columns    = Helper::getColumn($this->table);
        $cleanData  = [];
        foreach ($columns as $column)
        {
            if(isset($data[$column]))
            {
                $cleanData[$column] = $data[$column];
            }
        }
        return $cleanData;
    }
	
	function add($data)
	{	
		$clazzName  = $this->getClassName();
        $cleanData  = $this->getCleanData($data);
		$result     =  call_user_func([$clazzName,'create'],$cleanData);
		return $result;
	}

	function modify($data)
	{
	    $cleanData  = $this->getCleanData($data);
		$clazzName  = $this->getClassName();
		$obj        = call_user_func(array($clazzName,'where'),['id'=>$data['id']]);
		if($obj !=null)
		{
			return $obj->update($cleanData);
		}
		return 0;
	}

	function getById($id)
	{
		$clazzName = $this->getClassName();
		$obj = call_user_func(array($clazzName,'find'),[$id]);
		if(count($obj) > 0 )
		{
		    return $obj[0];
        }
		return null;
	}

	function getList($condition,$skip,$take)
	{
		$query =	DB::table($this->table)->select("*");
		$query =	$this->buildGetCondition($condition,$query);	

		return $query->get();
	}
	function listCount($condition){
		$query 		= DB::table($this->table);
		$query 		= $this->buildGetCondition($condition,$query);
		return $query->count();

	}

	function remove($id)
	{
		$clazzName = $this->getClassName();
		return call_user_func(array($clazzName,'destroy'),[$id]);
	}

	function getClassName()
	{
		return get_class($this);
	}
}