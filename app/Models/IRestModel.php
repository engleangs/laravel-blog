<?php
namespace Khmerblog\Models;
interface IRestModel
{
	function buildGetCondition($data,$query);
	function add($data);
	function modify($data);
	function getById($id);
	function getList($condition,$skip,$take);
	function listCount($condition);
	function remove($id);
}