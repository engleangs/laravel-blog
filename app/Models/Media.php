<?php
namespace Khmerblog\Models;
use Khmerblog\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use DB,Log;

define('MediaTypeFolder', 'folder');
define('MediaTypeFile', 'media');
class Media extends BaseModel
{
	protected $table = 'media';

	function buildGetCondition($data,$query)
	{
	    if(isset($data["parent_id"]))
	    {
	        $parent_id = (int) $data['parent_id'];
	        $query->where('parent_id',$parent_id);
        }
		return $query;
	}




}
