<?php
namespace Khmerblog\Models;
use Khmerblog\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use DB;
class AuditLog extends Model
{
	protected $table = 'logs';
	protected $guarded = [];
	public static function AddLog($tbl,$action,$desc,$ref,$data)
	{
	
		if(!is_string($data))
		{
			$data = json_encode($data);
		}
		AuditLog::create(['tbl'=>$tbl,'action'=>$action,
				'description'=>$desc,'ref'=>$ref,
				'data'=>$data, 'user_name'=>Helper::getUserName()
			]);
	}
	



}