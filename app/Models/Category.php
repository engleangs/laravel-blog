<?php

namespace Khmerblog\Models;
use Khmerblog\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use DB,Log;
class Category extends Model implements IRestModel
{
    //
    protected $table = 'categories';
    public function buildGetCondition($data,$query)
    {
    	

    	if(isset($data['name']))
    	{
    		$search = "%".$data['name'].'%';
    		$query->where('name','like',$search);
    	}


    	if(isset($data['language']))
    	{
    		$query->where('lang_id','=',$data['language']);
    	}
    	return $query;
    }

    public function adjustLeft($lft,$value)
    {
    	Category::where('lft','>=',$lft)
    		->increment('lft',$value);
    }
    public function adjustRight($rgt,$value)
    {
    	Category::where('rgt','>=',$rgt)
    			->increment('rgt',$value);
    }

    public function openHandToBottom($criteria,$value)
    {
    	$sql = "UPDATE {$this->table} SET  
    			rgt = rgt + {$value} WHERE  rgt >= {$criteria} and is_delete = 0";
    	Log::info($sql);
    	//echo $sql."\n";

    	DB::update($sql);
    	$sql = " UPDATE {$this->table} SET lft = lft + $value 
    			 WHERE lft >= {$criteria}  and is_delete = 0
    			";
    	Log::info($sql);
    //	echo $sql."\n";
    	DB::update($sql);
    		//exit();
    }

	public function add($data)
	{
		$index 			= 0;
		$id 			= 0;
		$parentId 		= 0;
		if(isset($data['parent_id']))
		{
			$parentId  	= $data['parent_id'];
			unset($data['parent_id']);
		}
		$parent 		= Category::where('id',$parentId)->get()->first();
		if (!$parent) 
		{
			throw new Exception("Invalid parent category", 1);
		}
		$this->openHandToBottom($parent->rgt,2);
		foreach ($data as $key => $value) 
		{
			if($index == 0)
			{
				$category 				= new Category();
				$category->name 		= isset($value['name'])?(string)$value['name']:'';
				$category->description 	= isset($value['description'])?(string)$value['description'] :'';
				$category->parent_id 	= $parentId;
				$category->lft 			= $parent->rgt;
				$category->rgt 			= $category->lft + 1;
				$category->level 		= $parent->level + 1;
				$category->lang_id 		= Helper::getLangId($key);
				$category->save();
				$id 					= $category->id;
				$value['id'] = $id;
				AuditLog::AddLog($this->table,'insert','add new category',$id,$value);

			}
			else
			{
				$category 				= new Category();
				$category->name 		= isset($value['name'])?(string)$value['name']:'';
				$category->description 	= isset($value['description'])?(string)$value['description'] :'';
				$category->lft 			= $parent->rgt;
				$category->rgt 			= $category->lft + 1;
				$category->level 		= $parent->level + 1;
				$category->parent_id 	= $parentId;
				$category->lang_id 		= Helper::getLangId($key);
				$category->id 			= $id;
				$category->save();
			}

			$index++;
		}
	}

	public function backupCurrentBranch($obj)
	{
		$sql = " UPDATE {$this->table} SET is_delete = -1 WHERE  lft >= {$obj->lft} 
				and rgt <= {$obj->rgt} AND  is_delete = 0 ";
		DB::update($sql);
	}
	public function restoreCurrentBranch($obj)
	{
		$sql = " UPDATE {$this->table} SET is_delete = 1 WHERE  lft >= {$obj->lft} and rgt <= {$obj->rgt} AND is_delete = -1 ";	
		DB::update($sql);
	}


	public function modify($data)
	{
		$id   =  0;
		if(isset($data['id']))
		{

			$id = $data['id'];
			unset($data['id']);	
		}

		$oldData 			= Category::where('id',$id)->first();
		$isUpdateParent 	= false;
		$newParent 			= null;
		$newParentId 		= $data['parent_id'];
		// perform cleaning data
		$languages = Helper::getLanguageDic();
		foreach ($data as $key => $value) 
		{
			if(!isset($languages[$key]))
			{
				unset($data[$key]); // remove unwanted data 
			}
		}

		//=======================================

		$value = [];
		foreach ($data as $key => $value) 
		{
			$lang 				= Helper::getLangId($key);
			$value['parent_id'] = $newParentId;
			Category::where('id',$id)
					->where('lang_id',$lang)
					->update($value);
		}


		if($newParentId 	!=$oldData->parent_id)
		{

			$this->backupCurrentBranch($oldData);
			$currentParent	  	= Category::where('id',$oldData->parent_id)->first();
			$diff 				= $oldData->rgt - $oldData->lft + 1;
			$this->openHandToBottom($oldData->rgt,-($diff) );
			$newParent 			=  Category::where('id',$newParentId)->first();
			$p 		 			= $newParent->rgt - $oldData->lft;
			$levelDiff 			= $newParent->level - $oldData->level + 1;
			$this->openHandToBottom($newParent->rgt,$diff);	
			$sql 				= " UPDATE {$this->table} set lft  = lft + {$p} , rgt = rgt + {$p} ,is_delete = 0, 
									level = level + {$levelDiff}
								WHERE is_delete  = -1 AND rgt <= $oldData->rgt AND lft >= $oldData->lft";
			DB::update($sql);
			$isUpdateParent 	= true;
		}

		

		AuditLog::AddLog($this->table,'update','update category',$id,$value);
	}

	public function getList($condition,$skip,$take)
	{

		$query  	= 	DB::table($this->table)->select("*",DB::raw("CONCAT( REPEAT('--',level -1),' ', name ) as tr_name"))
						->where('is_delete',0)
						->where('id','<>',1) // filter root out
						->orderBy('lft','ASC');
		$query		=	$this->buildGetCondition($condition,$query);
		return $query->skip($skip)->take($take)->get();
	}

	public function listCount($condition)
	{
		$query 		= DB::table($this->table)->where('is_delete',0);
		$query 		= $this->buildGetCondition($condition,$query);
		return 		$query->count();
	}

	public function remove($id)
	{
		$lst 		= Category::where('id',$id)->get();
		$obj 		= $lst->first();
		if($obj)
		{
			$sql 	= " UPDATE {$this->table} SET is_delete = 1 
						WHERE lft > {$obj->lft} AND rgt < {$obj->rgt} 
					";
			//echo $sql;

			DB::update($sql);
			Log::info($sql);

			$parent 	= Category::where('id', $obj->parent_id)->get()->first();	
			$diff 	 = - ( $obj->rgt - $obj->lft + 1 );

			Log::info( " diff :$diff ");
			$this->openHandToBottom($obj->rgt,$diff); // update parent

		}

		DB::table($this->table)->where('id',$id)->update(['is_delete'=>1]);
		AuditLog::AddLog($this->table,'delete','delete category',$id,$obj);
	}
    
    public function getListToBeParent($id)
    {
    	// echo $id;
    	// exit();

    	$defaultLangId = Helper::getDefaultLangId();
    	$query = DB::table($this->table)->select('id','name','level',DB::raw("CONCAT( REPEAT('--',level), ' ',name ) as tr_name"))
    			->where('lang_id','=',$defaultLangId)
    			->where('is_delete',0)
    			->where('id','<>',$id)
    			->orderBy("lft","asc");
    	$lst  	= Category::find($id);
    	$obj 	= $lst == null ? null :$lst->first();
    	$param = [];
    	if($obj)
    	{
    	
    		$query->where('rgt','<=',$obj->rgt);
    		$query->where('lft','>=',$obj->lft); // remove child tree
    		//echo  $obj->rgt;
    	}

    	// echo $query->toSql();
    	// exit();
    	return $query->get();

    }

    public function getById($id)
    {
    	$data 	= Category::where('id',$id)->get();
    	$result = [];
    	foreach ($data as $key => $value) 
    	{
    		$lang 		= Helper::getLangById($value->lang_id);
    		$langName 	= 'en';
    		if($lang)
    		{
    			$langName = $lang->name;
    		}

    		$result[ $langName ] = $value;
    	}
    	return $result;
    }
}
