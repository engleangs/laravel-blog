<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('front/home');
});
Route::get('/home', function () {
	return view('front/product');
});
Route::get('/slide', function () {
	return view('front/slidebar');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['prefix'=>"administrator","middleware"=>'web',"namespace"=>'Admin'],function(){
	Route::resource('post','PostController');
	Route::controller('home','MainController');
	Route::any('/','MainController@anyIndex');
	Route::controller('template','TemplateController');
	Route::resource('language','LanguageController');
	Route::get('category/parent/{id}','CategoryController@parent');
	Route::resource('category','CategoryController');
	Route::resource('media','MediaController');
	Route::resource('product','ProductController');
	Route::post('media/upload','MediaController@upload');
});
Route::group(['middleware' => ['web']], function () {
    //
});
