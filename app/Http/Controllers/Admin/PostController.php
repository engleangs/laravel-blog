<?php

namespace Khmerblog\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Khmerblog\Models\Post;
use Khmerblog\Http\Requests;
use Khmerblog\Http\Controllers\Controller;
use Cache;


class PostController extends Controller
{
    private $post;
    public function __construct(Post $post)
    {
    	$this->post = $post;
    }
    public function index()
    {
    	$time 	=   config('cache.time');
    	$post 	=  	$this->post;
    	$data 	= Cache::remember('posts',$time,function()use($post)
    	{
    		return $post->all();
    	});
    	return $data;
    }
}
