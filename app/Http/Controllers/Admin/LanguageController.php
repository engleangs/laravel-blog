<?php

namespace Khmerblog\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Khmerblog\Models\Language;
use Khmerblog\Http\Requests;
use Khmerblog\Http\Controllers\Controller;
use Cache,Session;


class LanguageController extends Controller
{
    private $languageModel;
    public function __construct(Language $model)
    {
    	$this->languageModel = $model;
    }

    public function index()
    {
    	$time 	=   config('cache.time');
        $model  =  $this->languageModel;
    	$data 	= Cache::remember('posts',$time,function()use($model)
    	{
            $results = [];
    		if(!Session::has('langauges'))
            {
                $results = $model->all();
                Session::put('langauges',$results);
            }
            $results = Session::get('langauges',[]);
            return $results;
    	});
    	return $data;
    }
}
