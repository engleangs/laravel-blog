<?php
namespace Khmerblog\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Khmerblog\Http\Requests;
use Khmerblog\Http\Controllers\Controller;
use Khmerblog\Models\Media;
use Khmerblog\Helpers\Helper,Khmerblog\Helpers\MediaHelper;
use Cache,Session,Validator,Exception,Log,File,Uuid,Storage,DB;
class MediaController extends ApiController
{

	protected $allowMimeTypes = [];
	protected $iconClass = '';
	protected $newFilePath = '';
	protected $isRenameFolder = false;
	protected $newFileName = '';
	protected $fileSize  = 0;
	protected function isAllowMimeType($mimeType)
	{
		return isset($this->allowMimeTypes[$mimeType]);
	}

	public function __construct(Media $model)
	{
		$this->model    = $model;
		$this->tagName  = 'Media';
	}
    protected function buildCondition($condition){
	    $data = [];
	    if(isset( $condition['parent_id'] ) )
	    {
	        $data["parent_id"] = (int) $condition["parent_id"];
        }
        return $data;
    }
	/**
	 @overide function get binding store
	*/
	protected function getBindingStore()
    {
		$data = $this->request->all();
        $parentObj = null;
        $data["parent_id"] = isset($data["parent_id"]) ? (int)$data["parent_id"] : 0;
        if($data['parent_id'] != 0 )
        {
            $parentObj  = $this->model->getById($data['parent_id']);
        }
        if($this->request->has('tmpPath') && $this->newFilePath !='')
        {
            $data['path'] = $this->newFilePath;
        }
        $mimeType = $this->request->input('mime_type','');

		if(!$this->request->has('type') || $this->request->input('type','media') == 'media')
		{
		    $type           = MediaHelper::getType($mimeType);
            $data['type']   = $type;
        }
        else{
		    $data['type']   = $this->request->input('type','folder');
        }
        //prepare for folder
        if( $data['type'] == MediaTypeFolder )
        {

            $data['path'] = $parentObj == null ? config("global.media_folder").'/'.$data['name'] :$parentObj->path.'/'.$data['name'];
        }
        $data["level"] = $parentObj == null ? 0 : $parentObj->level + 1;
        $data['file'] = $this->newFileName;
        $data['size'] = $this->fileSize;
		return $data;

	}


	/**
	@override function get binding update
	*/
	protected function getBindingUpdate()
	{
		$data               = $this->request->all();
        $parentObj          = null;
        $data["parent_id"]  = isset( $data["parent_id"] ) ? (int)$data["parent_id"] : 0;
        if($data['parent_id'] != 0 )
        {
            $parentObj      = $this->model->getById($data['parent_id']);
        }
		if( isset( $data['type'] ) )
		{
			unset( $data['type'] ); // not allow to update the type of media
		}

		if( $this->request->has( 'tmpPath' ) && $this->newFilePath !='' )
		{
            $data['path'] = $this->newFilePath;
        }
        if($this->isRenameFolder)
        {
            $parentObj = null;
            if( $data['parent_id'] != 0 )
            {
                $parentObj  = $this->model->getById( $data['parent_id'] );
            }
            $data['path'] = $parentObj == null ? config("global.media_folder").'/'.$data['name'] :$parentObj->path.'/'.$data['name'];
        }
        $data['file']   = $this->newFileName;
        $data["level"]  = $parentObj == null ? 0 : $parentObj->level + 1;
        $data['size']   = $this->fileSize;
		return $data;
	}

	protected function saveFile()
	{
		$tmpPath 		= $this->request->get('tmpPath');
		$tmpFileName    = basename($tmpPath);
		$publicStorage 	= Storage::disk('public');
		$path 			= 	config('global.media_folder');
		$folder 		= $this->request->input('folder',null);
		if( $folder != null )
		{
			$path 		= $folder;
		}
		$fullPath 		= public_path( $path );
		if( !file_exists( $fullPath ) )
		{
			throw new Exception("Can't find the destination folder .. {$path}", 1);
		}
		if( !$publicStorage->exists( $tmpPath ) )
		{
			throw new Exception("Can't find the fiel at {$tmpPath} to save.", 1);
		}
        $result         =  $path.'/'.$tmpFileName;
		$publicStorage->move($tmpPath,$result);
    	$this->fileSize =   $publicStorage->size($result);
		$this->newFilePath = $result;
		$this->newFileName = $tmpFileName;


		return $result;

	}

	protected function saveFolder()
    {
        $parent         = $this->request->input('folder','');
        $folderName     = $this->request->input('name','');
        $path 			= 	config('global.media_folder');
        if($parent !='')
        {
            $path       = $parent;

        }
        $fullPath       = public_path($path);
        if( !file_exists( $fullPath ) )
        {
            throw  new Exception("Can't find folder");
        }
        $path           = $path."/".$folderName;
        $fullPath       = public_path($path);
        if(file_exists($fullPath) &&is_dir($fullPath))
        {
            throw new Exception("Duplicated folder.");
        }
        mkdir($fullPath,0777,true);

    }

	public function store(Request $request)
	{
		$this->request = $request;
		try
		{
		    $mediaType = $request->input('type',null);
		    if($mediaType =='folder')
		    {
                $this->saveFolder();
            }
            else
            {

                $this->saveFile();
            }

			return parent::store($request); // call parent for reusable

		}catch(Exception $ex)
		{
			return["success"=>false,"message"=>$ex->getMessage()];
		}
		
	}

	protected function changeFileReference($item,$newPath)
	{
		$oldPath = $item->path;
		DB::table('media')->where( 'parent_id', $item->id )
            ->update( ['path'=>DB::raw("REPLACE( path,'$oldPath','$newPath' ) " ) ] );

	}

	public function update(Request $request,$id)
	{
		$this->request 	= $request ; // put reference to request to use it
        $media = config('global.media_folder');
		try 
		{
			$item 		= 	$this->model->getById($id);	
			if( $item == null )
			{
				throw new  Exception("Could not find media to update", 1);
			}
			$oldPath 	= $item->path;
			$newPath 	= $media.'/'.$request->input('name',$oldPath); // path of folder is name
			$type 		= $request->input('type',null);
			if($type == MediaTypeFolder)
			{
				$oldName 		= $item->name;
				$currentName 	= $request->input('name','');
				if($currentName =='')
				{
					throw new Exception("Invalid folder name", 1);
				}
				if($oldName 	!=	$currentName)
				{
				    $newFullPath = public_path($newPath);
				    if(file_exists($newFullPath) && is_dir($newFullPath))
				    {
				        throw new Exception("Duplicate folder");
                    }
                    $oldFullPath = public_path($oldPath);
				    if( file_exists( $oldFullPath )  && is_dir( $oldFullPath ) )
				    {
                        rename($oldFullPath, $newFullPath); //rename directory
                    }
                    else{
				        mkdir( $newFullPath );
                    }
					$this->changeFileReference($item,$newPath);
				    $this->isRenameFolder = true;
				}
			}
			else
			{
				// if type = file
				if($request->has('tmpPath'))
				{
					$this->saveFile();
				}
			}
			return parent::update($request,$id);

		} catch (Exception $e) 
		{
			return ["success"=>false , "message"=>$e->getMessage()];
		}
		

	}

	protected function checkFolder($path)
	{
		$folder = date("Y-m-d");
		if(!file_exists($path.'/'.$folder))
		{
			mkdir($folder);
		}
		return $folder;
	}

	public function upload(Request $request)
	{
		$file 			= $request->file('media');
		$mimeType 		= $file->getMimeType();
		$allowMimeTypes = MediaHelper::getAllowMimeType( $request );
		$data 			= [
		                    "success"        => true,
                            "message"       => "",
                            'mimeType'      => $mimeType,
							'size'          => $file->getClientSize(),
                            'clientName'    => $file->getClientOriginalName()
						];
		try 
		{
             if(!isset($allowMimeTypes[$mimeType]))
             {
                throw new Exception("File Type is not allow", 1);
             }
			$tmp 		= config('global.tmp_folder');
			$folderPath = public_path( $tmp );
			$extension  = $file->getClientOriginalExtension();
			$fileName 	=  Uuid::generate().'';
			$folderWrap = $this->checkFolder( $folderPath );
			$fileName 	= $fileName.'.'.$extension;
			$file->move( $folderPath.'/'.$folderWrap,$fileName );
			$data['fileName']	= $fileName;
			$data['folderPath']	= $tmp.'/'.$folderWrap;
			$data['type']		= MediaHelper::getType($mimeType);
			$data['url'] 		= asset( $tmp.'/'.$folderWrap.'/'.$fileName );
				
		} catch (Exception $e) 
		{
				$data["success"] = false;
				$data["message"] = $e->getMessage();
		}

		
		return $data;
	}




	public function indexFolder(Request $request)
	{
		$media = config('global.media_folder');
		$folderPath = public_path($media);
		$current = $request->get('current');
		$this->getAllowMimeType($request);//prepare for allowing mimeType to display
		$urlBasePath = asset($media);
		if($current)
		{
			$folderPath = $folderPath."/".$current;
			$urlBasePath = $urlBasePath."/".$current;
		}

		$mimeType = "";
		$data = [];

		if($dh = opendir($folderPath))
		{
			while (false !== ($entry = readdir($dh))) 
			{
				if($entry == "." || $entry == ".." || $entry ==".DS_Store")
				{
					continue;
				}
				$type = "file";
				$path = $folderPath."/".$entry;
				if(is_dir($folderPath."/".$entry))
				{
					$type = "folder";
				}
				else
				{
					$mimeType 		=	File::mimeType($path);
					$lastModified 	= 	File::lastModified($path);
					$extension 	 	= 	File::extension($path);

					if(!$this->isAllowMimeType($mimeType))
					{
						continue;
					}
				}

				$item = [ 
							"fileName"	=>$entry,
							"url"		=>$urlBasePath."/".$entry,
							"type" 		=>$type,
							"mimeType"  =>$mimeType,
						];
				$data[] = $item;
		    }
	
		}

		return ["result" => $data ]; 
	}

    public function destroy($id)
    {
        $data = [];
        try
        {
            $item   = $this->model->getById( $id );
            if( $item ==null )
            {
                throw new Exception( "Not media found to delete ." );
            }
            $full_path = public_path( $item->path );
            if( $item->type == MediaTypeFolder )
            {
                MediaHelper::recursiveDeleteItems($item);
                if( file_exists( $full_path ) && is_dir( $full_path ))
                {
                    rmdir( $full_path );
                }
            }
            else
            {
                if( file_exists( $full_path ) )
                {
                    unlink( $full_path );
                }
            }
            return parent::destroy($id);
        }catch ( Exception $exception )
        {
            $data["success"] = false;
            $data["message"] = $exception->getMessage();
        }
        return $data;

    }

}