<?php
namespace Khmerblog\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Khmerblog\Language;
use Khmerblog\Http\Controllers\Controller;
use Khmerblog\Helpers\Helper;
use Cache,Session,Validator,Log,Exception,Schema;
class ApiController extends Controller
{
	protected $validation = [] ;// need to add validation for check;
	protected $model; // need to inject model in custruct method
	protected $tagName;
    protected $request;
	protected function buildCondition($condition)
	{
		// need to overide this method
		return [];
	} 

    protected function getBindingStore()
    {
        return $this->request->all();
    }
    protected function getBindingUpdate()
    {
        return $this->request->all();
    }

	protected function getConditionHash($builtCondition)
	{
		$ctrlName = get_class($this);
		$strHash  = json_encode($builtCondition);
		return $ctrlName.md5($strHash);
		
	}
      /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $result                 = ['success'=>true, 'msg'=>''];
        try 
        {
             $data              =  $this->model->getById($id);
             $result['data']    = $data;

        } catch (Exception $e) 
        {
             $result['success'] = false;
             $result['msg'] = Helper::getErrorMsg($e);   
        }
        return $result;
         

    }

	public function index(Request $request)
    {
        try
        {

            $options        = $request->get('option',null);
            $options        = json_decode($options,true);
            $filter         = isset($options['filter'])?$options['filter']:[];
        	$condition 		= $this->buildCondition($filter);
        	$per_page 		= isset($options['per_page'])?$options['per_page']:20;
        	$start 			= isset($options['start'])?$options['start'] : 0;
        	$hashName 		= $this->getConditionHash($condition);
        	$time 			=  config('cache.time');
        	$model  		= $this->model;
            $lst            = $model->getList($condition,$start,$per_page);
            $total          = $model->listCount($condition);

        	// $data 			= Cache::remember($hashName,0.01,function()use($model,$condition,$start,$per_page)
    	    // {
	
				
         //        return          ['data'=>$lst, ' total'=>$total];
    	    // });
            $data           = ['data'=>$lst, 'total'=>$total];
        }catch(Exception $e)
        {
            Log::error($e);
            return ['success'=>false , 'message'=> Helper::getErrorMsg($e)];
        }
    	return $data;
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->request = $request;
    	$result 	= ['success'=>true,'message'=>'Saving '.$this->tagName.' success .'];
        $data       = $this->getBindingStore(); // allow sub class to override
        $validator = Validator::make($data, $this->validation);

        if ($validator->fails()) 
        {
             $messages 			= 	$validator->errors();
             $result['success']	= 	false;
             $result['message'] = 	$messages->toJson();
             return $result;
        }
        try
        {
        	$this->model->add( $data );	
         //    Log::info("Add new ".$this->tagName." ",$request->all());
        	 $result['message'] 	= $this->tagName.' has been created successfully';
        }
        catch(Exception $e)
        {
        	$result['success'] = false;
        	$result['message']	= Helper::getErrorMsg( $e );
            Log::error($e);
        }
        return $result;
        
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$this->request     = $request;
        $result 	       = ['success'=>true,'message'=>[]];
        $data              =  $this->getBindingUpdate();
        $validator = Validator::make($data, $this->validation);
        if ($validator->fails()) 
        {
             $messages 			= 	$validator->errors();
             $result['success']	= 	false;
             $result['message'] = 	$messages->toJson();
             return $result;
        }
        try
        {

        	if(!isset($data['id']))
        	{
        		$data['id'] = $id;
        	}
            Log::info(" update ".$this->tagName." ",$data);
        	$this->model->modify($data);	
        	$result['message'] 	= $this->tagName.' has been updated successfully';
        }
        catch(Exception $e)
        {
        	$result['success'] = false;
            Log::error($e);
        	$result['message']	= Helper::getErrorMsg($e);
        }
        return $result;

    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result 	= ['success'=>true,'message'=>[]];
        /*try
        {
            */
            Log::info(" delete ".$this->tagName." ".$id);
        	$this->model->remove($id);
        	$result['message'] 	= $this->tagName.' has been deleted successfully';
        // }
        // catch(Exception $e)
        // {
        // 	$result['success'] = false;
        // 	$result['message']	= Helper::getErrorMsg($e);
        //     Log::error($e);
        // }
        return $result;
    }



}