<?php

namespace Khmerblog\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Khmerblog\Http\Requests;
use Khmerblog\Http\Controllers\Controller;
use Khmerblog\Models\Category;
use Khmerblog\Helpers\Helper;
use Cache,Session,Validator,Exception,Log;
class CategoryController extends ApiController
{

    protected $validation = [
        'EN.name' => 'required|max:255',
        'KH.name' => 'required|max:200',
    ];
    public function __construct(Category $model)
    {
    	$this->model = $model;
    	$this->tagName  = "Category ";
    }

    public function buildCondition($condition)
    {
    	$newCondition = [];
    	if(isset($condition['language']))
    	{
    		$newCondition['language'] = (int)$condition['language'];
    	}
    	if(isset($condition['name']))
    	{
    		$newCondition['name'] 	= trim($condition['name']);
    	}
    	return $newCondition;
    }

    public function parent($id=0)
    {
        try 
        {
        
            $data   = $this->model->getListToBeParent($id);
            return [ 'data'=>$data,'success'=>true ];

        } catch (Exception $e) 
        {
            Log::error($e);
            return [ 'data'=>null, 'success'=>false,'message'=>Helper::getErrorMsg($e) ];    
        }
    }
}
