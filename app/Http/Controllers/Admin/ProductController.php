<?php
namespace Khmerblog\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Khmerblog\Http\Requests;
use Khmerblog\Http\Controllers\Controller;
use Khmerblog\Models\Product;
use Khmerblog\Helpers\Helper;
use Cache,Session,Validator,Exception,Log,File;
class ProductController extends ApiController
{
	public function __construct(Product $model)
	{
		$this->model = $model;
		$this->tagName = 'Product';
	}
}