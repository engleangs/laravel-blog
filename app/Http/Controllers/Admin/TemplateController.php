<?php
namespace Khmerblog\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Khmerblog\Http\Requests;
use Khmerblog\Http\Controllers\Controller;
use Khmerblog\Helpers\Helper,Khmerblog\Models\Language;
class TemplateController extends Controller
{
	public function anyIndex()
	{
		return view('admin.template.home');
	}

	public function anyCategory()
	{
		return view('admin.template.category');
	}
	public function anyEditCategory()
	{
		$languages = Helper::getLanguages();
		return view("admin.template.editCategory",['langs'=>$languages]);
	}
	
	public function anyMedia()
	{
		return view("admin.template.media");
	}
	public function anyEditMedia(){
		return view('admin.template.editMedia');
	}
	public function anyArticle()
	{
	    return view('admin.template.article');
    }

    public function anyView(Request $request)
    {
        $languages = Helper::getLanguages();
    	$template = $request->input('template','');
    	$templateUrl = 'admin.template.'.$template;
    	return view($templateUrl,['langs'=>$languages]);
    }

	
}