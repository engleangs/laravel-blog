<?php
namespace Khmerblog\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Khmerblog\Http\Requests;
use Khmerblog\Http\Controllers\Controller;
class MainController extends Controller
{
	public function anyIndex()
	{
		return view('admin.main');
	}
	public function anyLogin()
	{
		return view('admin.login');
	}
}