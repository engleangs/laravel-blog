<?php
/**
 * Created by PhpStorm.
 * User: angkor
 * Date: 1/14/17
 * Time: 12:17 AM
 */

namespace Khmerblog\Helpers;
use Cache,Session,Schema;
use Khmerblog\Models\Language;

class FormHelper
{
    public static function getFormCtrlRequireClass($form,  $ctrName )
    {
        return "{ 'has-feedback':  {$form}.{$ctrName}.".'$touched,'.
            "'has-error': ({$form}.\$submitted || {$form}.{$ctrName}.\$touched)&&{$form}.{$ctrName}.\$error.required,"
            ."'has-success': ({$form}.\$submitted || {$form}.{$ctrName}.\$touched)&&{$form}.{$ctrName}.\$valid"
            ."}";
    }
    public static function getFormRequireMsg($form, $ctrName,$msg)
    {
        return "<span class=\"help-block\" ng-show=\"({$form}.\$submitted || {$form}.{$ctrName}.\$touched)
                                    &&{$form}.{$ctrName}.\$error.required\">
                                      {$msg}
                                  </span>";
    }
    public static function  getFormRequireIcon($form,$ctrName)
    {
        return "<span class=\"form-control-feedback\"
                                        ng-show=\"({$form}.\$submitted || {$form}.{$ctrName}.\$touched)\">
                                      <i class=\"fa fa-times\" ng-show='{$form}.{$ctrName}.\$error.required'></i>
                                      <i class=\"fa fa-check\" ng-show='!{$form}.{$ctrName}.\$error.required'></i>
                                  </span>";
    }


}