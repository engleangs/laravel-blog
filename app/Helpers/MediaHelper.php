<?php
namespace Khmerblog\Helpers;
use Cache,Session;
use Khmerblog\Models\Media;

class MediaHelper
{
	public static function getAllowMimeType($type)
	{
		
		switch ($type) 
		{
			case 'image':	
				return 
					[
						'image/png'		=>'image/png',
						'image/jpg'		=>'image/jpg',
						'image/jpeg'	=>'image/jpeg',
						'image/bmp'		=>'image/bmp',
						'image/gif'		=>'image/gif',
					];
			case 'doc':
				return  
					[
						'application/msword'		=>'application/msword',
						'application/excel'			=>'application/excel',
						'application/x-msexcel'		=>'application/x-msexcel',
						'application/vnd.ms-excel'	=>'application/vnd.ms-excel',
						'application/xml'			=>'application/xml',
						'text/xml'					=>'text/xml',
						'text/html'					=>'text/html',
						'text/plain'				=>'text/plain',
						'application/pdf'			=>'application/pdf',
					];
				break;
			default:
				return
				[
					'image/png'			=>'image/png',
					'image/jpg'			=>'image/jpg',
					'image/jpeg'		=>'image/jpeg',
					'image/bmp'			=>'image/bmp',
					'image/gif'			=>'image/gif',
					'application/msword'=>'application/msword',
					'application/excel'	=>'application/excel',
					'application/x-msexcel'		=>'application/x-msexcel',
					'application/vnd.ms-excel'	=>'application/vnd.ms-excel',
					'application/xml'	=>'application/xml',
					'text/xml'			=>'text/xml',
					'text/html'			=>'text/html',
					'application/pdf'	=>'application/pdf',
					'text/plain'		=>'text/plain',
				];
				break;
		}

	}

	public static function getType($mimeType)
	{
		switch ($mimeType) 
		{
			case 	'image/png':
			case	'image/jpg':
			case	'image/jpeg':
			case	'image/bmp':
			case	'image/gif':

										return 'image';


			case 	'application/msword'	:
			case 	'application/excel'		:
			case 	'application/x-msexcel'	:
			case 	'application/vnd.ms-excel'	:
			case 	'application/xml'		:
			case 	'text/xml'				:
			case 	'text/html'				:
			case 	'text/plain'			:
			case 	'application/pdf'		:

											return 'doc';
			default:
									return 'unknown';
		}
	}

	public static function recursiveDeleteItems(Media $item){
	    $media      = config("global.media_folder");
	    $items      = Media::where("parent_id",$item->id)->get();
	    foreach ($items as $i=>$child)
	    {
            $full_path  = public_path($media."/".$item->path);
            if($item->type = MediaTypeFolder)
            {
                MediaHelper::recursiveDeleteItems($item); //call to itself , recrusive to delete items inside sub folder
                if( file_exists( $full_path) && is_dir( $full_path ) )
                {
                    rmdir($full_path); //remove directory
                }

            }
            else{
                if( file_exists($full_path) ){
                    unlink($full_path); // remove file
                }
            }
            $item->delete(); // delete item from table
        }

    }
}