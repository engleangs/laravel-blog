<?php
namespace Khmerblog\Helpers;
use Cache,Session,Schema;
use Khmerblog\Models\Language;

class Helper
{
	public static function getLanguages()
	{
		if(!Session::has('languages'))
		{
			$data = Language::all();
			$languages = [];
			foreach ($data as $key => $value) 
			{
				$languages[$value->id] = $value;
			}
			Session::put('languages',$languages);
		}

		return Session::get('languages',[]);

	}

	public static function getLanguageDic()
	{
		if(!Session::has('languages_dic'))
		{
			$languages = self::getLanguages();
			$newData 	= [];
			foreach ($languages as $key => $value) 
			{
				$newData[$value->name] = $value;
			}
			Session::put('languages_dic',$newData);
		}
		return Session::get('languages_dic',[]);
	}
	
	public static function getLangId($langName)
	{
		$id =  0;
		$langDic  	= self::getLanguageDic();
		if(isset($langDic[$langName]))
		{
			$id = $langDic[$langName]->id;
		}
		return $id;
	}
	public static function getLangById($id)
	{
		$languages = self::getLanguages();
		if(isset($languages[$id]))
		{
			return $languages[$id];
		}
		return null;
	}	

	public static function getUserName()
	{
		return "";
	}

	public static function getDefaultLangId()
	{
		return 1;
	}

	public static function getErrorMsg($e)
	{
		if(config('app.debug'))
		{
			return $e->getMessage() ."<br/>".$e->getTraceAsString();
		}
		
		return "Sorry unexpected error";
	}

	public static function getColumn($table)
    {
        //cache it in session
        if(Session::has('schema'))
        {
            $schema = Session::get('schema');
            if(isset($schema[$table]))
            {
                return $schema[$table];
            }
            else {
                $columns = Schema::getColumnListing($table);
                $schema[$table] = $columns;
                Session::put('schema',$schema);
                return $columns;

            }

        }
        else{
            $schema = [];
            $columns = Schema::getColumnListing($table);
            $schema[$table] = $columns;
            Session::put('schema',$schema);
            return $columns;
        }

    }



}