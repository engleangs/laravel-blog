<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>
        <link rel="stylesheet" type="text/css" href="{{asset("assets/css/bootstrap.css")}}" />
        <link rel="stylesheet" type="text/css" href="{{asset("assets/font-awesome/css/font-awesome.css")}}" >
        <link rel="stylesheet" type="text/css" href="{{asset("assets/font-awesome/css/font-awesome-ie7.min.css")}}" >
        <link rel="stylesheet" type="text/css" href="{{asset("assets/style.css")}}" />
    </head>
    <body>
        <div class="container">
            <header id="header"></header>
            <div class="content">
                <div class="content-product">
                    @section('sidebar')
                        @include('front.slidebar')
                    @show
                    @yield('content')
                    @section('footer')
                        @include('front.footer')
                    @show
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <p class="pull-right">
                    <a href="#"><img src="{{asset("assets/img/maestro.png")}}" alt="payment"></a>
                    <a href="#"><img src="{{asset("assets/img/mc.png")}}" alt="payment"></a>
                    <a href="#"><img src="{{asset("assets/img/pp.png")}}" alt="payment"></a>
                    <a href="#"><img src="{{asset("assets/img/visa.png")}}" alt="payment"></a>
                    <a href="#"><img src="{{asset("assets/img/disc.png")}}" alt="payment"></a>
                </p>
                <span>Copyright &copy; 2013<br> Khmer Blog</span>
            </div>
        </div>
    </body>
    <a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
    <script rel="stylesheet" src="{{asset("assets/js/jquery.js")}}"></script>
    <script rel="stylesheet" src="{{asset("assets/js/bootstrap.min.js")}}"></script>
    <script rel="stylesheet" src="{{asset("assets/js/jquery.easing-1.3.min.js")}}"></script>
    <script rel="stylesheet" src="{{asset("assets/js/jquery.scrollTo-1.4.3.1-min.js")}}"></script>
    <script rel="stylesheet" src="{{asset("assets/js/shop.js")}}"></script>
    <script rel="stylesheet" src="{{asset("http://html5shim.googlecode.com/svn/trunk/html5.js")}}"></script>
</html>