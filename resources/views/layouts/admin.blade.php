<!DOCTYPE html>
<html lang="en" ng-app="KhmerBlog">	
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
		<title>
			Angular Blog | 
		</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="{{asset("admin/assets/css/vendor.css")}}" />		
		<link rel="stylesheet" href="{{asset("admin/assets/css/app-green.css")}}" />
		<link rel="stylesheet" href="{{asset("admin/assets/css/style.css")}}" />
		<link rel="stylesheet" type="text/css" 
		href="{{asset("admin/assets/components/ng-table/dist/ng-table.min.css")}}">
		<script type="text/javascript" src="{{asset("admin/assets/components/angular/angular.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/components/angular-resource/angular-resource.min.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/components/angular-route/angular-route.min.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/components/ng-table/dist/ng-table.min.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/components/ui-select/dist/select.min.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/components/ckeditor/ckeditor.js")}}"></script>
		<link rel="stylesheet" type="text/css" href="{{asset("admin/assets/components/ui-select/dist/select.min.css")}}">
		<script type="text/javascript" 
				src="{{asset("admin/assets/components/angular-bootstrap/ui-bootstrap.min.js")}}"></script>
		<script type="text/javascript" 
			src="{{asset("admin/assets/components/angular-bootstrap/ui-bootstrap-tpls.js")}}"></script>
			
		<link rel="stylesheet" type="text/css" href="{{asset("admin/assets/components/alertify.js/dist/css/alertify.css")}}">
		<script type="text/javascript" src="{{asset("admin/assets/components/alertify.js/dist/js/alertify.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/components/alertify.js/dist/js/ngAlertify.js")}}"></script>
		<link rel="stylesheet" type="text/css" href="{{asset("admin/assets/components/font-awesome/css/font-awesome.min.css")}}">
		<script type="text/javascript" src="{{asset("admin/assets/components/jquery/dist/jquery.min.js")}}"></script>
		<script type="text/javascript">
			var _baseURL = '{{asset("/")}}';
		String.prototype.endsWith = function(str) 
			{
				return (this.match(str+"$")==str)
			};
			String.prototype.startsWith = function(str) {return (this.match("^"+str)==str)};
			
		</script>
		<script type="text/javascript" src="{{asset("admin/assets/js/controller/app.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/controller/controller.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/controller/CategoryCtrl.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/controller/MediaCtrl.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/controller/ArticleCtrl.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/service/app.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/service/service.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/service/category.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/service/media.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/service/article.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/directive/bind-file.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/directive/media-wrapper.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/directive/media-type.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/directive/editor.js")}}"></script>
		<script type="text/javascript" src="{{asset("admin/assets/js/directive/scroll.js")}}"></script>
		
	</head>
<body class="" ng-controller="KhmerBlogCtrl">
	<nav class="navbar topnav-navbar navbar-fixed-top" role="navigation">
<div class="navbar-header text-center">
	<button type="button" class="navbar-toggle" id="showMenu" >
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>

	<a class="navbar-brand" href="home.html">
		Angular Blog
	 </a>
</div>
<div class="collapse navbar-collapse">
	<form class="navbar-form navbar-left" role="search">
		<span class="glyphicon glyphicon-search"></span>
		<div class="form-group">
			<input type="text" class="form-control" placeholder="">
		</div>
	</form>
	<ul class="nav navbar-nav">
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><span class="glyphicon glyphicon-envelope"></span><span class="badge badge-green">5</span></a>
			<ul class="dropdown-menu animated fadeIn">
				<li class="messages-top text-center">
					.topnav1
				</li>
				<li class="dropdown-messages">
					<a href="#">
						<div class="message-sender">
							.lucy
						</div>
						<div class="message-header">
							.topnavheader1
						</div>
					</a>
				</li>
				<li class="dropdown-messages">
					<a href="#">
						<div class="message-sender">
							.diptesh
						</div>
						<div class="message-header">
							.topnavheader2
						</div>
					</a>
				</li>
				<li class="dropdown-messages">
					<a href="#">
						<div class="message-sender">
							.weng
						</div>
						<div class="message-header">
							.topnavheader3
						</div>
					</a>
				</li>
				<li class="dropdown-messages">
					<a href="#">
						<div class="message-sender">
							.jade
						</div>
						<div class="message-header">
							.topnavheader4
						</div>
					</a>
				</li>
			</ul>
		</li>
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
				<span class="glyphicon glyphicon-bell"></span><span class="badge badge-red">13</span>
			</a>
			<ul class="dropdown-menu animated fadeIn">
				<li class="messages-top text-center">
					.threenotifications
				</li>
				<li class="dropdown-notifications">
					<a href="#">
						<div class="notification">
							<i class="fa fa-thumbs-up"></i>
							.runuma
						</div>
					</a>
				</li>
				<li class="dropdown-notifications">
					<a href="#">
						<div class="notification">
							<i class="fa fa-thumbs-up"></i>
							.harshita
						</div>
					</a>
				</li>
				<li class="dropdown-notifications">
					<a href="#">
						<div class="notification">
							<i class="fa fa-user-plus"></i>
							.archana
						</div>
					</a>
				</li>
				<li class="dropdown-notifications">
					<a href="#">
						<div class="notification">
							<i class="fa fa-user-times"></i>
							.animesh
						</div>
					</a>
				</li>
			</ul>
		</li>
	</ul>
	<ul class="nav navbar-nav pull-right navbar-right">

		<li class="dropdown">
			<a href="#" target="_self"
				class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
				<span>.lang</span>
			</a>
			<ul class="dropdown-menu lang pull-right fadeIn">
				<li><a href="#" onclick="changeLanguage('en')" class="lang">English</a></li>
				<li><a href="#" onclick="changeLanguage('de')" class="lang">Dutch</a></li>
				<li><a href="#" onclick="changeLanguage('ur')" class="lang">اردو</a></li>
				<li><a href="#" onclick="changeLanguage('hn')" class="lang">हिन्दी</a></li>
			</ul>
		</li>

		<li class="dropdown admin-dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
				<img src="../public/admin/assets/images/flat-avatar.png" class="topnav-img" alt=""><span class="hidden-sm">Ani Pascal</span>
			</a>
			<ul class="dropdown-menu" role="menu">
				<li><a href="profile.html">.profilee</a></li>
				<li><a href="login.html">.logout</a></li>
			</ul>
		</li>
	</ul>


</div>
<ul class="nav navbar-nav pull-right hidd">
	<li class="dropdown admin-dropdown" dropdown on-toggle="toggled(open)">
		<a href class="dropdown-toggle animated fadeIn" dropdown-toggle>
			<img src="/public/admin/assets/images/flat-avatar.png" class="topnav-img" alt=""></a>
		<ul class="dropdown-menu pull-right">
			<li><a href="profile.html">profile</a></li>
			<li><a href="login.html">logout</a></li>
		</ul>
	</li>
</ul>
</nav>
	<aside id="sidebar">
		<div class="sidenav-outer">
			<div class="side-menu">
				<div class="menu-body">
					<ul class="nav nav-pills nav-stacked sidenav">
						<li >
							<a href="home.html">
							<span class="glyphicon glyphicon-home"></span>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a href="#">
									Dashboard
								</a></li>
							</ul>
						</li>
						<li >
							<a href="typography.html">
							<span class="fa fa-cog"></span>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a  href="#/category">Category</a></li>
							</ul>
						</li>
						<li >
							<a href="#/media">
							<span class="glyphicon glyphicon-th-large"></span>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a href="#/media">Media Manager</a></li>
							</ul>
						</li>
						<li >
							<a href="#/article">
							<span class="glyphicon glyphicon-list"></span>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a  href="#/article">Articel</a></li>
							</ul>
						</li>
						<li >
							<a href="form-elements.html">
								<span class="glyphicon glyphicon-equalizer"></span>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li class="sidemenu-header">.form</li>
								<li><a href="form-elements.html">.elements</a></li>
								<li><a href="form-components.html">.components</a></li>
							</ul>
						</li>
						<li >
							<a href="button.html">
							<span class="glyphicon glyphicon-cloud-download"></span>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li class="sidemenu-header">.user_interface</li>
								<li><a href="button.html">.buttons</a></li>
								<li><a href="dropdown.html">.dropdown</a></li>
								<li><a href="icons.html">.icons</a></li>
								<li><a href="panels.html">.panels</a></li>
								<li><a href="alerts.html">.alerts</a></li>
								<li><a href="progressbars.html">.progressbars</a></li>
								<li><a href="pagination.html">.pagination</a></li>
								<li><a href="other-elements.html">.other_elements</a></li>
							</ul>
						</li>
						<li >
							<a href="chartjs.html">
							<span class="glyphicon glyphicon-signal"></span>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li class="sidemenu-header">.charts</li>
								<li><a href="chartjs.html">Chart</a></li>
								<li><a href="c3chart.html">C3Chart</a></li>
							</ul>
						</li>
						<li >
							<a href="calendar.html">
							<span class="glyphicon glyphicon-calendar"></span>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a href="calendar.html">.calendar</a></li>
							</ul>
						</li>
						<li >
							<a href="inbox.html">
							<span class="glyphicon glyphicon-envelope"></span>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li class="sidemenu-header">.mail</li>
								<li><a href="inbox.html">.inbox</a></li>
								<li><a href="compose.html">.compose</a></li>
							</ul>
						</li>
						<li >
							<a href="invoice.html">
							<span class="glyphicon glyphicon-list-alt"></span>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a href="invoice.html">.invoice</a></li>
							</ul>
						</li>
						<li >
							<a href="docs.html">
							<span class="glyphicon glyphicon-credit-card"></span>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li><a href="docs.html">Docs</a></li>
							</ul>
						</li>
						<li >
							<a href="blank.html">
							<span class="glyphicon glyphicon-duplicate"></span>
							</a>
							<ul class="nested-dropdown animated fadeIn">
								<li class="sidemenu-header">.pages</li>
								<li><a href="blank.html">.blankpage</a></li>
								<li><a href="login.html">.login</a></li>
								<li><a href="signup.html">.singup</a></li>
								<li><a href="404-page.html">.404page</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<div class="side-widgets">
		<div class="widgets-content">
			<div class="text-center">
				<a href="profile.html">
					<img src="/public/admin/assets/images/flat-avatar.png" class="user-avatar" /></a>
				<div class="text-center avatar-name">
					Ani Pascal
				</div>
			</div>

			<div class="calendar-container text-center" >
				<div id="calendar2" class="fc-header-title"></div>
			</div>

			<div class="news-feed">
				<div class="feed-header">.feed</div>
				<div class="feed-content">
					<ul class="feed">
						<li>
							<a href="#">.li1</a> <span class="feed-date">25/4/2015</span>
						</li>
						<li>
							<a href="#">.li2</a> <span class="feed-date">25/4/2015</span>
						</li>
						<li>
							<a href="#">.li3</a> <span class="feed-date">25/4/2015</span>
						</li>
						<li>
							<a href="#">.li4</a> <span class="feed-date">25/4/2015</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
		</div>
</aside>
<section id="body-container" class="animsition dashboard-page">

	<div class="conter-wrapper home-container" ng-view>

	</div>
</section>

<div class="default-loader" ng-show="showLoader">
</div>
<script src="{{asset("admin/assets/js/vendor.js")}}"></script>
<script type="text/javascript">
	//var bodyContainerScrollObserable = [];
	window.bodyContainerScrollObserable = window.bodyContainerScrollObserable || []; // create scroll observable;
	function scrollToID(id, speed)
	{
		var offSet = 50;
		var targetOffset = $(id).offset().top - offSet;
		var mainNav = $('#main-nav');
		$('html,body').animate({scrollTop:targetOffset}, speed);
		if (mainNav.hasClass("open")) {
			mainNav.css("height", "1px").removeClass("in").addClass("collapse");
			mainNav.removeClass("open");
		}

	}

	$('#body-container').scroll(function (e) 
	{
		//console.log( 'window height ',$(window).height());
		var top = $(this).scrollTop();
		//console.log( 'this top ', $(this).scrollTop() );
		//console.log( 'this offset ',$(this).offset());
		//console.log( $('#myTab').scrollTop(),'mytab');
		//console.log( $('#editor-wrapper2').position() ,'editor');
		var bodyContainerScrollObserable  = window.bodyContainerScrollObserable || [];
		var that = this;
		for( var i in bodyContainerScrollObserable)
		{
			var observable  = bodyContainerScrollObserable[i];
			if(observable && angular.isFunction(observable))
			{
				observable(that, e,top); // callback to observable
			}

		}
	});
</script>
	</body>

	@yield('content')
	<footer>
		&copy; Khmerblog.org
	</footer>
</body>
</html>