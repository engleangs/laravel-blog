@extends('layouts.layout')
@section('content')
    <div class="span9 content-right">
        <div class="well well-small">
            <h3>New Products
                <p class="pull-right">
                    <span>Showing: 60 of 392</span>
                    <a class="btn-control" href="#"></i> <span class="icon-chevron-left"></span></a>
                    <a class="btn-control" href="#"></i> <span class="icon-chevron-right"></span></a>
                </p>
            </h3>
            <h4>
                <ul class="sub-header">
                    <li class="sub-header-detail">
                        BRAND:
                        <a>ALL BRAND</a>
                    </li>
                    <li class="sub-header-detail">BRAND:
                        <a>ALL BRAND</a>
                    </li>
                    <li class="sub-header-detail">BRAND:
                        <a>ALL BRAND</a>
                    </li>
                </ul>
            </h4>
            <hr class="soften"/>
            <div class="row-fluid product-list">
                <ul class="thumbnails list-product">
                    <li class="span4">
                        <div class="product-thumbnail">
                            <div class="thumbnail">
                                <a href="#">
                                    <img src="assets/img/b.jpg" alt="">
                                </a>
                                <div class="caption cntr">
                                    <p>Manicure & Pedicure</p>
                                    <h4>$4.00</h4>
                                    <br class="clr">
                                </div>
                            </div>
                            <a class="zoomTool" href="#" title="add to cart">
                                <span class="icon-shopping-cart"></span>
                                ADD TO CARD
                            </a>
                        </div>
                    </li>
                    <li class="span4">
                        <div class="product-thumbnail">
                            <div class="thumbnail">
                                <a href="#"><img src="assets/img/c.jpg" alt=""></a>
                                <div class="caption cntr">
                                    <p>Manicure & Pedicure</p>
                                    <h4>18.00</h4>
                                    <br class="clr">
                                </div>
                            </div>
                            <a class="zoomTool" href="#" title="add to cart" style="display: none;">
                                <span class="icon-shopping-cart"></span>
                                ADD TO CARD
                            </a>
                        </div>
                    </li>
                    <li class="span4">
                        <div class="product-thumbnail">
                            <div class="thumbnail">
                                <a href="#">
                                    <img src="assets/img/a.jpg" alt="">
                                </a>
                                <div class="caption cntr">
                                    <p>Manicure & Pedicure</p>
                                    <h4>$20.00</h4>
                                    <br class="clr">
                                </div>
                            </div>
                            <a class="zoomTool" href="#" title="add to cart" style="display: none;">
                                <span class="icon-shopping-cart"></span>
                                ADD TO CARD
                            </a>
                        </div>
                    </li>
                    <li class="span4">
                        <div class="product-thumbnail">
                            <div class="thumbnail">
                                <a href="#"><img src="assets/img/d.jpg" alt=""></a>
                                <div class="caption cntr">
                                    <p>Manicure &amp; Pedicure</p>
                                    <h4>$22.00</h4>
                                    <br class="clr">
                                </div>
                            </div>
                            <a class="zoomTool" href="#" title="add to cart" style="display: none;">
                                <span class="icon-shopping-cart"></span>
                                ADD TO CARD
                            </a>
                        </div>
                    </li>
                    <li class="span4">
                        <div class="product-thumbnail">
                            <div class="thumbnail">
                                <a href="#"><img src="assets/img/e.jpg" alt=""></a>
                                <div class="caption cntr">
                                    <p>Manicure &amp; Pedicure</p>
                                    <h4>$22.00</h4>
                                    <br class="clr">
                                </div>
                            </div>
                            <a class="zoomTool" href="#" title="add to cart" style="display: none;">
                                <span class="icon-shopping-cart"></span>
                                ADD TO CARD
                            </a>
                        </div>
                    </li>
                    <li class="span4">
                        <div class="product-thumbnail">
                            <div class="thumbnail">
                                <a href="#">
                                    <img src="assets/img/f.jpg" alt=""></a>
                                <div class="caption cntr">
                                    <p>Manicure &amp; Pedicure</p>
                                    <h4>$22.00</h4>
                                    <br class="clr">
                                </div>
                            </div>
                            <a class="zoomTool" href="#" title="add to cart" style="display: none;">
                                <span class="icon-shopping-cart"></span>
                                ADD TO CARD
                            </a>
                        </div>
                    </li>
                    <li class="span4">
                        <div class="product-thumbnail">
                            <div class="thumbnail">
                                <a href="#" class="overlay"></a>
                                <a href="#"><img src="assets/img/g.jpg" alt=""></a>
                                <div class="caption cntr">
                                    <p>Manicure &amp; Pedicure</p>
                                    <h4>$22.00</h4>
                                    <br class="clr">
                                </div>
                            </div>
                            <a class="zoomTool" href="#" title="add to cart" style="display: none;">
                                <span class="icon-shopping-cart"></span>
                                ADD TO CARD
                            </a>
                        </div>
                    </li>
                    <li class="span4">
                        <div class="product-thumbnail">
                            <div class="thumbnail">
                                <a href="#" class="overlay"></a>
                                <a href="#"><img src="assets/img/h.jpg" alt=""></a>
                                <div class="caption cntr">
                                    <p>Manicure &amp; Pedicure</p>
                                    <h4>$22.00</h4>
                                    <br class="clr">
                                </div>
                            </div>
                            <a class="zoomTool" href="#" title="add to cart" style="display: none;">
                                <span class="icon-shopping-cart"></span>
                                ADD TO CARD
                            </a>
                        </div>
                    </li>
                    <li class="span4">
                        <div class="product-thumbnail">
                            <div class="thumbnail">
                                <a href="#" class="overlay"></a>
                                <a href="#"><img src="assets/img/i.jpg" alt=""></a>
                                <div class="caption cntr">
                                    <p>Manicure &amp; Pedicure</p>
                                    <h4>$22.00</h4>
                                    <br class="clr">
                                </div>
                            </div>
                            <a class="zoomTool" href="#" title="add to cart" style="display: none;">
                                <span class="icon-shopping-cart"></span>
                                ADD TO CARD
                            </a>
                        </div>
                    </li>
                    <li class="span4">
                        <div class="product-thumbnail">
                            <div class="thumbnail">
                                <a href="#" class="overlay"></a>
                                <a href="#"><img src="assets/img/h.jpg" alt=""></a>
                                <div class="caption cntr">
                                    <p>Manicure &amp; Pedicure</p>
                                    <h4>$22.00</h4>
                                    <br class="clr">
                                </div>
                            </div>
                            <a class="zoomTool" href="#" title="add to cart" style="display: none;">
                                <span class="icon-shopping-cart"></span>
                                ADD TO CARD
                            </a>
                        </div>
                    </li>
                    <li class="span4">
                        <div class="product-thumbnail">
                            <div class="thumbnail">
                                <a href="#" class="overlay"></a>
                                <a href="#"><img src="assets/img/i.jpg" alt=""></a>
                                <div class="caption cntr">
                                    <p>Manicure &amp; Pedicure</p>
                                    <h4>$22.00</h4>
                                    <br class="clr">
                                </div>
                            </div>
                            <a class="zoomTool" href="#" title="add to cart" style="display: none;">
                                <span class="icon-shopping-cart"></span>
                                ADD TO CARD
                            </a>
                        </div>
                    </li>
                    <li class="span4">
                        <div class="product-thumbnail">
                            <div class="thumbnail">
                                <a href="#" class="overlay"></a>
                                <a href="#"><img src="assets/img/h.jpg" alt=""></a>
                                <div class="caption cntr">
                                    <p>Manicure &amp; Pedicure</p>
                                    <h4>$22.00</h4>
                                    <br class="clr">
                                </div>
                            </div>
                            <a class="zoomTool" href="#" title="add to cart" style="display: none;">
                                <span class="icon-shopping-cart"></span>
                                ADD TO CARD
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection