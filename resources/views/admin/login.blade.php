<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from ani-laravel.strapui.com/login by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 06 Feb 2016 15:13:15 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
		<title>Khmerblog | 	Login
</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="{{asset("admin/assets/css/vendor.css")}}" />		
		<link rel="stylesheet" href="{{asset("admin/assets/css/app-green.css")}}" />
	</head>
	<body class="">
			<div class="login-page">
		<div class="row">
			<div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
	        	<img src="{{asset("admin/assets/images/logo.png")}}" />
				
				<form role="form" action="{{action("Admin\MainController@anyIndex")}}">
		<div class="form-content" >
			<div class="form-group">
				<input type="text" class="form-control input-underline input-lg" id="" placeholder=.email>
			</div>
			<div class="form-group">
				<input type="password" class="form-control input-underline input-lg" id="" placeholder=.password>
			</div>
		</div>
		<input type="submit" class="btn btn-white btn-outline btn-lg btn-rounded progress-login" value=".login" />
		&nbsp;
		<a href="signup.html" class="btn btn-white btn-outline btn-lg btn-rounded">.register</a> 
	</form>
			</div>			
		</div>
	</div>
			
			<script src="{{asset("admin/assets/js/vendor.js")}}"></script>
			</body>
</html>