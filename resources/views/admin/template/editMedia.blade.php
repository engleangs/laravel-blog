<form name="mediaForm" role="form" novalidate ng-show="type=='media'">
<div class="modal-header">
    <h3 class="modal-title">
        @{{getModuleTitle()}}
    </h3>
</div>
<div class="modal-body">
    <div class="form-horizontal" >
    <div class="">
         <div>
          <div class=" margin-top-05">
          <div class="form-group"
             ng-class="{ 'has-feedback':  mediaForm.Name.$touched,
                            'has-error': (mediaForm.$submitted || mediaForm.Name.$touched)&&mediaForm.Name.$error.required,
                            'has-success': (mediaForm.$submitted || mediaForm.Name.$touched)&&mediaForm.Name.$valid
                        }
             ">
                      <label class="control-label col-sm-4 errors" for="name">
                         Name
                      </label>
                      <div class="col-sm-8">
                          <input type="text" ng-model="selectedMedia.name"
                                 class="form-control"
                                 name="Name" placeholder="Enter Name" required="required">
                          <span class="form-control-feedback"
                                ng-show="(mediaForm.$submitted || mediaForm.Name.$touched)
                                &&mediaForm.Name.$error.required">
                              <i class="fa fa-times"></i>
                          </span>
                          <span class="form-control-feedback"
                                ng-show="(mediaForm.$submitted || mediaForm.Name.$touched)
                                        &&mediaForm.Name.$valid">
                              <i class="fa fa-check"></i>
                          </span>
                          <span class="help-block" ng-show="(mediaForm.$submitted || mediaForm.Name.$touched)
                            &&mediaForm.Name.$error.required">
                              Media Name is required
                          </span>
                      </div>
            </div>
            
          <div class="form-group">
            <div class="col-sm-8 pull-right">
              <button class="btn btn-primary col-sm-6" type="button" ng-click="uploadFile()" > 
                <i class="fa fa-plus-square" aria-hidden="true"></i> Upload Media File </button> 
                <div class="col-sm-10">
                  <div id="mediaWrap" media-wrapper-directive ng-model="mediaItem">
                  
                  </div>  
                </div>
                
            </div>
             
          </div>
         
                  <div class="form-group">
                      <label class="control-label col-sm-4" for="description">Description :</label>
                      <div class="col-sm-8">
                        <textarea class="form-control" 
                          rows="10" cols="30"
                          ng-model="selectedMedia.description"
                          placeholder="Enter Category Description"></textarea>
                      </div>
                  </div>
                 </div>
               </div>
         
        </div>
        
      </div>  
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="submit" name="save" ng-click="mediaForm.$invalid;ok(mediaForm)"> 
      <i class="fa fa-save"></i> Save 
    </button>
    <button class="btn btn-default" type="button" name="cancel" ng-click="cancel()">
        <i class="fa fa-times" ></i>
        Cancel
    </button>
</div>
</form>
<div style="display: none;">
  <form id="upload-form" enctype="multipart/form-data">
  <input bind-file-directive type="file" name="fileUpload" id="file-media" ng-model="fileUpload" 
  file-change="uploadFileChange($event,files)">
</form>

  
</div>

<form name="folderForm" role="form" novalidate ng-show="type=='folder'">
  <div class="modal-header">
      <h3 class="modal-title">
          @{{getModuleTitle()}}
      </h3>
  </div>
  <div class="modal-body">
     <div class=" margin-top-05">
            <div class="form-group"
               ng-class="{ 'has-feedback':  mediaForm.Name.$touched,
                              'has-error': (mediaForm.$submitted || mediaForm.Name.$touched)&&mediaForm.Name.$error.required,
                              'has-success': (mediaForm.$submitted || mediaForm.Name.$touched)&&mediaForm.Name.$valid
                          }
               ">
                        <label class="control-label col-sm-4 errors" for="name">
                           Folder Name
                        </label>
                        <div class="col-sm-8">
                            <input type="text" ng-model="selectedMedia.name"
                                   class="form-control"
                                   name="Name" placeholder="Enter Name" required="required">
                            <span class="form-control-feedback"
                                  ng-show="(mediaForm.$submitted || mediaForm.Name.$touched)
                                  &&mediaForm.Name.$error.required">
                                <i class="fa fa-times"></i>
                            </span>
                            <span class="form-control-feedback"
                                  ng-show="(mediaForm.$submitted || mediaForm.Name.$touched)
                                          &&mediaForm.Name.$valid">
                                <i class="fa fa-check"></i>
                            </span>
                            <span class="help-block" ng-show="(mediaForm.$submitted || mediaForm.Name.$touched)
                              &&mediaForm.Name.$error.required">
                                Media Name is required
                            </span>
                        </div>
              </div>
      </div>
      <div class="clearfix"></div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" type="submit" name="save" ng-click="folderForm.$invalid;ok(mediaForm)"> 
      <i class="fa fa-save"></i> Save 
    </button>
    <button class="btn btn-default" type="button" name="cancel" ng-click="cancel()">
        <i class="fa fa-times" ></i>
        Cancel
    </button>
  </div>
</div>
</form>
Type : 
@{{type}}