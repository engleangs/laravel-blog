<form name="categoryForm" role="form" novalidate>
<div class="modal-header">
    <h3 class="modal-title">
        @{{getModuleTitle()}}
    </h3>
</div>
<div class="modal-body">
    <div class="form-horizontal" >
      <div class="">
                <ul id = "myTab" class = "nav nav-tabs">
                  <?php $strClass = 'active' ?>
                  @foreach($langs as $i=>$language)

                   <li class="{{$strClass}}">
                      <a href = "#tab-{{$language->name}}" 
                        data-toggle ="tab" target="_self">
                         {{$language->description}}
                      </a>
                   </li>
                   <?php 
                    $strClass = '';
                    ?>
                  @endforeach
                  
                </ul>

                <div   class = "tab-content">

                  <?php 
                      $strClass = " in active";
                  ?>
                  @foreach($langs as $i=>$language)
                      
              
                           <div class = "tab-pane  {{$strClass}}"   
                            id ="tab-{{$language->name}}" >
                            <div class=" margin-top-05">
                           <div class="form-group"
                               ng-class="{ 'has-feedback':  categoryForm.Name{{$language->name}}.$touched,
                                              'has-error': (categoryForm.$submitted || categoryForm.Name{{$language->name}}.$touched)&&categoryForm.Name{{$language->name}}.$error.required,
                                              'has-success': (categoryForm.$submitted || categoryForm.Name{{$language->name}}.$touched)&&categoryForm.Name{{$language->name}}.$valid
                                          }
                               ">
                              <label class="control-label col-sm-4 errors" for="name">
                                  Name ( {{$language->name}}) * :
                              </label>
                              <div class="col-sm-8">
                                  <input type="text" ng-model="selectedCategory.{{$language->name}}.name"
                                         class="form-control"
                                         name="Name{{$language->name}}" placeholder="Enter Category" required="">
                                  <span class="form-control-feedback"
                                        ng-show="(categoryForm.$submitted || categoryForm.Name{{$language->name}}.$touched)
                                        &&categoryForm.Name{{$language->name}}.$error.required">
                                      <i class="fa fa-times"></i>
                                  </span>
                                  <span class="form-control-feedback"
                                        ng-show="(categoryForm.$submitted || categoryForm.Name{{$language->name}}.$touched)
                                                &&categoryForm.Name{{$language->name}}.$valid">
                                      <i class="fa fa-check"></i>
                                  </span>
                                  <span class="help-block" ng-show="(categoryForm.$submitted || categoryForm.Name{{$language->name}}.$touched)
                                    &&categoryForm.Name{{$language->name}}.$error.required">
                                      Category Name is required
                                  </span>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-sm-4" for="email">Description ({{$language->name}}) :</label>
                              <div class="col-sm-8">
                                <textarea class="form-control" 
                                  rows="10" cols="30"
                                  ng-model="selectedCategory.{{$language->name}}.description"
                                  placeholder="Enter Category Description"></textarea>
                              </div>
                          </div>
                         </div>
                       </div>
                   <?php $strClass = "" ?>

                  @endforeach
                  
                </div>
        
      </div>  
      <div class="line">
      </div>
      
      <div class="form-group">
                 <label class="control-label col-sm-4" for="email"> Parent Category</label>
                    <div class="col-sm-8">
                      <ui-select  ng-model="parentCategory.selected" >
                        <ui-select-match placeholder="Select Parent ">
                            <span ng-bind="$select.selected.tr_name"></span>
                        </ui-select-match>
                        <ui-select-choices repeat="item in (lstParent.data | filter: $select.search) track by item.id">
                            <span ng-bind="item.tr_name"></span>
                        </ui-select-choices>
                      </ui-select>
                    </div>
              </div>  
   <!--   <div class="">
        <div class="">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Parent Info</h3>
            </div>
            <div class="panel-body" style="min-height:100px;">
              

            </div>
          </div>
        </div>
      </div>
       <div class="clearfix">

       </div>     
        -->
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="submit" name="save" ng-click="categoryForm.$invalid;ok(categoryForm)"> 
      <i class="fa fa-save"></i> Save 
    </button>
    <button class="btn btn-default" type="button" name="cancel" ng-click="cancel()">
        <i class="fa fa-times" ></i>
        Cancel
    </button>
</div>
</form>
