<div class="col-md-12" ng-controller="MediaCtrl as jt">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Media 
              <button type="button" class="btn btn-xs btn-primary pull-right" ng-click="addFolder()">
                <i class="fa fa-plus"></i>  Add New Folder
              </button>
              <button type="button" class="btn btn-xs btn-primary pull-right" ng-click="addMedia()">
                <i class="fa fa-plus"></i>  Add New Media
              </button>
            </h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-4">
                <div class="input-group"> 
                    <input type="text" class="form-control" placeholder="Search by name" 
                    ng-model="filter.name"> 
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                      </button>
                    </span>
                </div>
              </div>
              <div class="col-sm-8 pull-right">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item" ng-class="{ 'active' : parent_id == 0 }" >
                    <a ng-if="currentFolders.length >  0" href="javascript:void(0);" ng-click="navigateToFolder(null,true)">Root</a>
                    @{{ parent_id == 0 ? "Root " : ''}}

                  </li>
                  <li class="breadcrumb-item" ng-repeat=" folder in currentFolders " 
                    ng-class="{ 'active' : parent_id == folder.id}">
                    <a ng-if="parent_id !=folder.id" ng-click="navigateToFolder(folder,false)">
                      @{{folder.name}}
                    </a>
                    @{{ folder.id == parent_id ? folder.name : '' }}
                  </li>
                  <!-- <li class="breadcrumb-item"><a href="#">Library</a></li>
                  <li class="breadcrumb-item active">Data</li> -->
                </ol>
              </div>
            </div>
            <div loading-container="jt.tableParams.settings().$loading" class="margin-top-05">
                      <table ng-table="jt.tableParams" class="table table-bordered table-striped table-condensed">
                          <tr ng-repeat="row in $data">
                              <td width="10%" data-title="'Id'" filter="{id: 'number'}" sortable="'id'">@{{row.id}}</td>
                              <td data-title="'Name'" filter="{name: 'text'}" sortable="'name'">
                              <div ng-if="row.type=='folder'">
                                  <a href="javascript:void(0);" title="Browse Folder" ng-click="gotoFolder(row)">@{{row.name}} </a> 
                              </div>
                              <div ng-if="row.type !='folder'">
                                @{{row.name}}
                              </div>
                              </td>
                              <td data-title="'Type'">
                                <div ng-model="row"   style="width: 70px" media-type-directive folder-click="gotoFolder">
                                </div>
                              </td>

                              <td data-title="'Description'" >
                                  @{{row.description}}
                              </td>

                              <td data-title="'Action'" width="10%">
                                  <button class="btn btn-xs btn-default" uib-tooltip="Edit" ng-click="editMedia(row)">
                                      <i class="fa fa-edit"></i>
                                  </button>
                                  <button class="btn btn-xs btn-default" uib-tooltip="Delete" ng-click="deleteMedia(row)">
                                      <i class="fa fa-remove"></i>
                                  </button>
                              </td>


                          </tr>
                          <tr ng-show="$data.length==0">
                              <td colspan="5">
                                  No media or subfolder found.
                              </td>
                          </tr>

                      </table>
                <!-- /.table-responsive -->
                  </div>
          </div>
        </div>
      </div>