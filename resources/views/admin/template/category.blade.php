<div class="col-md-12" ng-controller="CategoryCtrl as jt">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Category 
							<button type="button" class="btn btn-xs btn-primary pull-right" ng-click="addCategory()">
								<i class="fa fa-plus"></i>	Add New Category</button>
						</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-4">
								<div class="input-group"> 
										<input type="text" class="form-control" placeholder="Search by Category name" 
										ng-model="filter.name"> 
										<span class="input-group-btn">
											<button class="btn btn-default" type="button">
												<i class="fa fa-search"></i>
											</button>
										</span>
								</div>
							</div>
							<div class="col-sm-4 pull-right">
								<div class="input-group">
									<ui-select allow-clear ng-model="filter.language" on-select="onSelectLanguage($item, $model)" >
									    <ui-select-match placeholder="Select Langauge">
									        <span ng-bind="$select.selected.description"></span>
									    </ui-select-match>
									    <ui-select-choices repeat="item in (languages | filter: $select.search) track by item.id">
									        <span ng-bind="item.description"></span>
									    </ui-select-choices>
									</ui-select>
									<span class="input-group-btn">
						              <button ng-click="onClearSelectLanguage()" class="btn btn-default">
						                <span class="fa fa-times"></span>
						              </button>
						            </span>

								</div>
							</div>
						</div>
						<div loading-container="jt.tableParams.settings().$loading" class="margin-top-05">
			                <table ng-table="jt.tableParams" class="table table-bordered table-striped table-condensed">
			                    <tr ng-repeat="row in $data">
			                        <td width="10%" data-title="'Id'" filter="{id: 'number'}" sortable="'id'">@{{row.id}}</td>
			                        <td data-title="'Name'" filter="{name: 'text'}" sortable="'name'">@{{row.tr_name}}</td>
			                        <td data-title="'Action'" width="10%">
			                            <button class="btn btn-xs btn-default" uib-tooltip="Edit" ng-click="editCategory(row)">
			                                <i class="fa fa-edit"></i>
			                            </button>
			                            <button class="btn btn-xs btn-default" uib-tooltip="Delete" ng-click="deleteCategory(row)">
			                                <i class="fa fa-remove"></i>
			                            </button>
			                        </td>

			                    </tr>
			                    <tr ng-show="$data.length==0">
			                        <td colspan="3">
			                            No result found.
			                        </td>
			                    </tr>
			                </table>
                <!-- /.table-responsive -->
            			</div>
					</div>
				</div>
			</div>