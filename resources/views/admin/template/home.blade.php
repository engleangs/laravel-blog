
		<div class="row home-row">
			<div class="col-md-4 col-lg-3">
				<div class="home-stats">
					<a href="c3chart.html" class="stat hvr-wobble-horizontal">
	<div class=" stat-icon">
		<i class="fa fa-cloud-upload fa-4x text-info "></i>
	</div>
	<div class=" stat-label">
		<div class="label-header">
			88%
		</div>
		<div class="progress-sm progress ng-isolate-scope" value="progressValue" type="info">
		  	<div class="progress-bar progress-bar-info" role="progressbar"
		   		aria-valuenow="88" aria-valuemin="0" aria-valuemax="100"  style="width: 88%;">
		   	</div>
		</div>  
		<div class="clearfix stat-detail">	
			<div class="label-body">
				<i class="fa fa-arrow-circle-o-right pull-right text-muted"></i>.stat1
			</div>
		</div>
	</div>
</a>
<a href="c3chart.html" class="stat hvr-wobble-horizontal">
	<div class=" stat-icon">
		<i class="fa fa-heartbeat fa-4x text-success "></i>
	</div>
	<div class=" stat-label">
		<div class="label-header">
			94%
		</div>
		<div class="progress-sm progress ng-isolate-scope" value="progressValue" type="info">
		  	<div class="progress-bar progress-bar-success" role="progressbar"
		   		aria-valuenow="94" aria-valuemin="0" aria-valuemax="100"  style="width: 94%;">
		   	</div>
		</div>  
		<div class="clearfix stat-detail">	
			<div class="label-body">
				<i class="fa fa-arrow-circle-o-right pull-right text-muted"></i>.stat2
			</div>
		</div>
	</div>
</a>					<a href="inbox.html" class="stat hvr-wobble-horizontal">
	<div class=" stat-icon">
		<i class="fa fa-flag fa-4x text-danger "></i>
	</div>
	<div class=" stat-label">
		<div class="label-header">
			88%
		</div>
		<div class="progress-sm progress ng-isolate-scope" value="progressValue" type="info">
		  	<div class="progress-bar progress-bar-danger" role="progressbar"
		   		aria-valuenow="88" aria-valuemin="0" aria-valuemax="100"  style="width: 88%;">
		   	</div>
		</div>  
		<div class="clearfix stat-detail">	
			<div class="label-body">
				<i class="fa fa-arrow-circle-o-right pull-right text-muted"></i>.stat3
			</div>
		</div>
	</div>
</a>		
				</div>
			</div>
			<div class="col-md-4 col-lg-6">
				<div class="home-charts-middle">
					<div class="chart-container">
						<div class="chart-comment clearfix">
							<div class="text-primary pull-left">
								<span class="comment-header">55%</span><br />
								<span class="comment-comment">.chart3</span>
							</div>
							<div class="text-success pull-left m-l">
								<span class="comment-header">25%</span><br />
								<span class="comment-comment">.chart2</span>
							</div>
							<div class="text-warning pull-left m-l">
								<span class="comment-header">20%</span><br />
								<span class="comment-comment">.chart1</span>
							</div>
						</div>
						<div id="lineChart" style="height:250px"></div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-lg-3">
				<div class="home-charts-right">
					<div class="top-right-chart">
						<div id="cbar" style="height:150px; padding-top:7px;"></div>
					</div>
					<div class="bottom-right-chart">
						<div class="top-left-chart clearfix">
							<div class="row">
								<div class="col-sm-6 text-left">
									<div class="padder">
										<span class="heading">.views : </span><br />
										<big class="text-primary">22068</big>
									</div>
								</div>
								<div class="col-sm-6">
									<div id="pie"  style="height:125px; padding-top:8px;"></div>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<div class="row home-row">
			<div class="col-lg-8 col-md-6">
				<div class="map-container box padder">
					<div id="world-map" style="width: 100%; height: 320px"></div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="todo-container panel panel-danger">
					<div class="panel-heading">
						<div class="todo-header text-center">
							<h4><i class="fa fa-tasks"></i>&nbsp;.todo</h4>
						</div>
					</div>
					<div class="panel-body bg-danger">
						<div class="todo-body">
							<div class="todo-list-wrap">
								<ul class="todo-list">
									<li class="">
										<label class="checkbox1" for="option1"> 
											<input id="option1" type="checkbox" class="">
											<span></span>
										</label>
										<span class="done-false">Meeting with Nabindar Singh.</span>
									</li>
									<li class="">
										<label class="checkbox1" for="option3"> 
											<input id="option3" type="checkbox" class="">
											<span></span>
										</label>
										<span class="done-false">Exercise at 6:pm with Nicholas.</span>
									</li>
									<li class="">
										<label class="checkbox1" for="option4"> 
											<input id="option4" type="checkbox" class="">
											<span></span>
										</label>
										<span class="done-false">Avengers Age of Ultron.</span>
									</li>
									<li class="">
										<label class="checkbox1" for="option5"> 
											<input id="option5" type="checkbox" class="">
											<span></span>
										</label>
										<span class="done-false">Henna birthday at Mezbaan.</span>
									</li>
									<li class="">
										<label class="checkbox1" for="option2"> 
											<input id="option2" type="checkbox" class="">
											<span></span>
										</label>
										<span class="done-false">Black Magic</span>
									</li>
								</ul>
							</div>
							<form class="form-horizontal todo-from-bottom">
								<div class="row">
									<div class="col-sm-12">
										<div class="input-group">
											<input type="text" class="form-control" placeholder="">
											<span class="input-group-btn">
												<button class="btn btn-default" type="submit">.add</button>
											</span>
								    	</div>	
								    </div>				
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>