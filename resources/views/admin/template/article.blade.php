<div class="col-md-12" ng-controller="ArticleCtrl as jt">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Article
                <button type="button" class="btn btn-xs btn-primary pull-right" ng-click="addArticle()">
                    <i class="fa fa-plus"></i>  Add New Article
                </button>

            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search by title"
                               ng-model="filter.name">
                        <span class="input-group-btn">
                      <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                      </button>
                    </span>
                    </div>
                </div>
                <div class="col-sm-8 pull-right">

                </div>
            </div>
            <div loading-container="jt.tableParams.settings().$loading" class="margin-top-05">
                <table ng-table="jt.tableParams" class="table table-bordered table-striped table-condensed">
                    <tr ng-repeat="row in $data">
                        <td width="10%" data-title="'Id'" filter="{id: 'number'}" sortable="'id'">@{{row.id}}</td>
                        <td data-title="'Title'" filter="{name: 'text'}" sortable="'title'">
                            @{{row.title}}
                        </td>
                        <td data-title="'Summary'">
                            @{{ row.summary }}
                        </td>

                        <td data-title="'Created Date'" filter="{created_by:'text'}" >
                            @{{ row.created_at }}
                        </td>
                        <td data-title="'Updated Date'">
                            @{{ row.updated_at }}
                        </td>
                        <td data-title="'View No'">
                            @{{ row.view_no }}
                        </td>
                        <td data-title="'Total Comment'">
                            @{{ row.total_comment }}
                        </td>

                        <td data-title="'Action'" width="10%">
                            <button class="btn btn-xs btn-default" uib-tooltip="Edit" ng-click="editMedia(row)">
                                <i class="fa fa-edit"></i>
                            </button>
                            <button class="btn btn-xs btn-default" uib-tooltip="Delete" ng-click="deleteMedia(row)">
                                <i class="fa fa-remove"></i>
                            </button>
                        </td>


                    </tr>
                    <tr ng-show="$data.length==0">
                        <td colspan="5">
                            No article found.
                        </td>
                    </tr>

                </table>
                <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>