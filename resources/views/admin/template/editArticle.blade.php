<div class="col-md-12" ng-controller="ArticleCtrl as jt">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Article
                <div id="btn-top-group">
                <button type="button" id="btn-cancel" class="btn btn-xs btn-primary pull-right" ng-click="addArticle()">
                    <i class="fa fa-times"></i>  Cancel
                </button>
                <button type="button"  class="btn btn-xs btn-primary pull-right" ng-click="addArticle()">
                    <i class="fa fa-save"></i>  Save
                </button>
                
                </div>

            </h3>
        </div>
        <div class="panel-body">
        <form name="articleForm" role="form" novalidate>
          <div class="form-horizontal" >
            <div class="scroll-wrapper" scroll-wrapper-directive>
              <div class="scroll-head">
                <nav class="navbar nav-scroll">
                  <div class="container-fluid">
                    <div>
                      <div class="collapse navbar-collapse" id="scroll-head-bar">
                        <ul class="nav navbar-nav">
                          <li class="active"><a href="javascript:void(0);" data-target="generial-info">Content</a></li>
                          <li><a href="javascript:void(0);" data-target="editor-wrapper"> Category </a></li>
                          <li><a href="javascript:void(0);" data-target="editor-wrapper2"> Configuration </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </nav>    
                </div>
                <div style="padding:30px;"></div>

              <div class="scroll-item" id="generial-info" >
              <div> <h3> Content  </h3></div>
                        <ul id = "myTab" class = "nav nav-tabs">
                          <?php $strClass = 'active' ?>
                          @foreach($langs as $i=>$language)

                           <li class="{{$strClass}}">
                              <a href = "#tab-{{$language->name}}"
                                data-toggle ="tab" target="_self">
                                 {{$language->description}}
                              </a>
                           </li>
                           <?php
                            $strClass = '';
                            ?>
                          @endforeach

                        </ul>

                        <div   class = "tab-content">

                          <?php
                              $strClass = " in active";
                          ?>
                          @foreach($langs as $i=>$language)

                                   <div class = "tab-pane  {{$strClass}}"
                                    id ="tab-{{$language->name}}" >
                                    <div class=" margin-top-05">
                                   <div class="form-group"
                                       ng-class="<?php echo \Khmerblog\Helpers\FormHelper::getFormCtrlRequireClass('articleForm','title'.$language->name) ?>">
                                      <label class="control-label col-sm-3 errors" for="name">
                                          Name ( {{$language->name}}) * :
                                      </label>
                                      <div class="col-sm-8">
                                          <input type="text" ng-model="selectedCategory.{{$language->name}}.name"
                                                 class="form-control"
                                                 name="Name{{$language->name}}" placeholder="Enter Category" required="">

                                         <?php echo \Khmerblog\Helpers\FormHelper::getFormRequireIcon("articleForm","title".$language->name) ?>
                                         <?php echo  \Khmerblog\Helpers\FormHelper::getFormRequireMsg("articleForm","title".$language->name,"Title is required") ?>

                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="control-label col-sm-3" for="email">Summary ({{$language->name}}) :</label>
                                      <div class="col-sm-9">
                                        <textarea class="form-control"
                                          rows="3" cols="30"
                                          ng-model="selectedCategory.{{$language->name}}.description"
                                          placeholder="Enter Category Description"></textarea>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="control-label col-sm-3" for="email">Content ({{$language->name}}) :</label>
                                      <div class="col-sm-9">
                                        <textarea ng-model="content[{{$language->name}}]" 
                                          name="content[{{$language->name}}]" 
                                          id="cotent_{{$language->name}}"
                                          ckeditor-directive></textarea>
                                      </div>
                                  </div>
                                 </div>
                               </div>
                           <?php $strClass = "" ?>

                          @endforeach
                        </div>

              </div>
              
              <div class="line">
              </div>
              <div id="category">
                <h3> Category </h3>
                 <div class="form-group">
                  <label class="control-label col-sm-3">
                    Select Article Category
                  </label>
                  <div class="col-sm-9">
                  <div class="col-md-12">
                     <div>
                      <div class="pull-left col-sm-8">
                        <ul class="pagination pagination-sm" style="margin:2px;">
                        <li ng-class="{'disabled': !categoryData.has_previous }"> 
                          <a href="javascript:void(0);" title="Previous" 
                            ng-click="categoryPagePrevious()" 
                            > <i class="fa fa-arrow-left"></i></a></li>
                        <li ng-class="{'disabled': !categoryData.has_next }" ng-click="categoryPageNext()">
                        <a  title="Next"  ><i class="fa fa-arrow-right"></i></a></li>
                      </ul>
                      </div>
                      <div class="pull-right col-sm-3">
                        <div class="form-group has-feedback">
                          <input type="text" class="form-control" ng-model="categoryData.filter" id="inputSuccess2"/>
                          <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                      </div>
                    </div>
                     <table class="table table-bordered">
                     <thead>
                       <tr>
                         <th> Categoy Name</th>
                         <th>  # </th>
                       </tr>

                     </thead>
                      <tbody>
                        <tr ng-repeat="category in categoryData.data.data">
                          <td>
                               @{{category.name}}  
                          </td>
                          <td>
                          <input type="checkbox" name="" ng-checked="selectedCategory[category.id]">
                          </td>
                        </tr>
                      </tbody>
                     </table>
                  </div>
                   
                  </div>
                 </div>
              </div>
              <div class="line">
              </div>
              <div id="feature-image">
                <h3> Configuration </h3>
                  <div class="form-group">
                    <label class="control-label col-sm-3">
                      Upload / Select Feature Image 
                    </label>
                    <div class="col-sm-9">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-3">
                       Is Feature
                    </label>
                    <div class="col-sm-9">
                      <div class="onoffswitch">
                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch3" checked="">
                        <label class="onoffswitch-label" for="switch3">
                          <span class="onoffswitch-inner"></span>
                          <span class="onoffswitch-switch"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-3">
                       Allow Comment
                    </label>
                    <div class="col-sm-9">
                      <div class="onoffswitch">
                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch4" >
                        <label class="onoffswitch-label" for="switch4">
                          <span class="onoffswitch-inner"></span>
                          <span class="onoffswitch-switch"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-3">
                      Status 
                    </label>
                    <div class="col-sm-9">
                      <ui-select  ng-model="parentCategory.selected" >
                        <ui-select-match placeholder="Select Article Status ">
                            <span ng-bind="$select.selected.name"></span>
                        </ui-select-match>
                        <ui-select-choices repeat="item in (status | filter: $select.search) track by item.id">
                            <span ng-bind="item.name"></span>
                        </ui-select-choices>
                      </ui-select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-3">
                      Related Article
                    </label>
                    <div class="col-sm-9">
                      <div class="col-sm-12" ng-hide="!articles || articles.length ==0">
                        <div class="pull-left col-sm-8">
                        <ul class="pagination pagination-sm" style="margin:2px;">
                        <li ng-class="{'disabled': !categoryData.has_previous }"> 
                          <a href="javascript:void(0);" title="Previous" 
                            ng-click="categoryPagePrevious()" 
                            > <i class="fa fa-arrow-left"></i></a></li>
                        <li ng-class="{'disabled': !categoryData.has_next }" ng-click="categoryPageNext()">
                        <a  title="Next"  ><i class="fa fa-arrow-right"></i></a></li>
                      </ul>
                      </div>
                      <div class="pull-right col-sm-3">
                        <div class="form-group has-feedback">
                          <input type="text" class="form-control" ng-model="categoryData.filter" id="inputSuccess2"/>
                          <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                      </div>
                      </div>
                      <div class="clearfix"></div>
                      <ul class="list-group">
                        <li class="list-group-item" ng-repeat=" items in articles">
                            <input type="checkbox" name="">
                              Related Article 1
                        </li>

                        <li class="list-group-item" ng-show="articles.length==0 || !articles">
                            No Article Found 

                        </li>
                      </ul>
                    </div>
                  </div>
              </div>



             
            </div>
          </div>
        </form>
        </div>
    </div>
</div>
